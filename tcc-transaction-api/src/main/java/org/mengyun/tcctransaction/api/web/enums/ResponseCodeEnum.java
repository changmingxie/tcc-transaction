package org.mengyun.tcctransaction.api.web.enums;


/**
 * @Author huabao.fang
 * @Date 2022/05/19 17:25
 **/
public enum ResponseCodeEnum {

    SUCCESS(200, "成功"),
    REQUEST_PARAM_ERROR(400, "参数错误"),
    UNKOWN_ERROR(500, "未知异常"),
    NOT_SUPPORT(405, "不支持"),
    NOT_SUPPORT_WITH_MESSAGE(405, "%s不支持"),


    // 登录相关
    LOGIN_USER_NOT_EXST(10200, "用户不存在"),
    LOGIN_PASSWORD_ILLEGAL(10201, "密码有误"),
    LOGIN_ERROR(10209, "登录失败"),
    LOGIN_ERROR_WITH_MESSAGE(10209, "登录失败:%s"),

    // 任务管理相关
    TASK_OPERATE_NOT_SUPPORT(10300, "任务操作不支持"),


    ;

    ResponseCodeEnum(int responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    private int responseCode;
    private String responseMessage;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public String getResponseMessage(Object... args) {
        try {
            return String.format(this.responseMessage, args);
        } catch (Exception e) {
            return this.responseMessage;
        }
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCode() {
        return String.valueOf(this.responseCode);
    }

    public String getMessage() {
        return this.responseMessage;
    }
}
