package org.mengyun.tcctransaction.api.web.dto;

import javax.transaction.xa.Xid;
import java.util.Date;

/**
 * @Author huabao.fang
 * @Date 2022/5/25 14:14
 **/
public class TransactionStoreDto {

    private String domain;
    private Xid xid;
    private Xid rootXid;
    private byte[] content;
    private Date createTime = new Date();
    private Date lastUpdateTime = new Date();
    private long version = 0l;
    private int retriedCount = 0;
    private int statusId;
    private int transactionTypeId;

    private String xidString;

    private String rootXidString;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Xid getXid() {
        return xid;
    }

    public void setXid(Xid xid) {
        this.xid = xid;
    }

    public Xid getRootXid() {
        return rootXid;
    }

    public void setRootXid(Xid rootXid) {
        this.rootXid = rootXid;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public int getRetriedCount() {
        return retriedCount;
    }

    public void setRetriedCount(int retriedCount) {
        this.retriedCount = retriedCount;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(int transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getXidString() {
        return xidString;
    }

    public void setXidString(String xidString) {
        this.xidString = xidString;
    }

    public String getRootXidString() {
        return rootXidString;
    }

    public void setRootXidString(String rootXidString) {
        this.rootXidString = rootXidString;
    }
}
