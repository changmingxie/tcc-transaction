package org.mengyun.tcctransaction.http.springmvc.interceptor;

import com.alibaba.fastjson.JSON;
import org.mengyun.tcctransaction.api.TransactionContext;
import org.mengyun.tcctransaction.context.TransactionContextHolder;
import org.mengyun.tcctransaction.http.constants.TransactionContextConstants;
import org.mengyun.tcctransaction.utils.StringUtils;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;

public class RequesterInterceptor implements HandlerInterceptor {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String transactionContext = request.getHeader(TransactionContextConstants.TRANSACTION_CONTEXT);
        if (StringUtils.isNotEmpty(transactionContext)) {
            TransactionContextHolder.setCurrentTransactionContext(JSON.parseObject(new String(Base64.getDecoder().decode(transactionContext)), TransactionContext.class));
        }

        return true;
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        String transactionContext = request.getHeader(TransactionContextConstants.TRANSACTION_CONTEXT);
        if (StringUtils.isNotEmpty(transactionContext)) {
            TransactionContextHolder.clear();
        }
    }
}
