package org.mengyun.tcctransaction.http.feign.aspect;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.mengyun.tcctransaction.context.ThreadLocalTransactionContextEditor;
import org.mengyun.tcctransaction.interceptor.AspectJTransactionMethodJoinPoint;
import org.mengyun.tcctransaction.interceptor.ResourceCoordinatorAspect;
import org.mengyun.tcctransaction.support.FactoryBuilder;

@Aspect
public class FeignCoordinatorAspect {

    @Pointcut("@within(org.springframework.cloud.openfeign.FeignClient) && @annotation(org.mengyun.tcctransaction.api.EnableTcc)")
    public void transactionResourcePointcut() {
    }

    @Around("transactionResourcePointcut()")
    public Object interceptTransactionResourceMethodWithCompensableAnnotation(ProceedingJoinPoint pjp) throws Throwable {
        return FactoryBuilder.factoryOf(ResourceCoordinatorAspect.class).getInstance().interceptTransactionContextMethod(new AspectJTransactionMethodJoinPoint(pjp, null, ThreadLocalTransactionContextEditor.class));
    }
}
