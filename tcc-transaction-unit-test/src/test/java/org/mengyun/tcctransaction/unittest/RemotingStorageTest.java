package org.mengyun.tcctransaction.unittest;


import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mengyun.tcctransaction.ClientConfig;
import org.mengyun.tcctransaction.ServerConfig;
import org.mengyun.tcctransaction.TccServer;
import org.mengyun.tcctransaction.common.TransactionType;
import org.mengyun.tcctransaction.remoting.netty.NettyRemotingClient;
import org.mengyun.tcctransaction.remoting.netty.ServerAddressLoader;
import org.mengyun.tcctransaction.serializer.*;
import org.mengyun.tcctransaction.storage.RemotingTransactionStorage;
import org.mengyun.tcctransaction.storage.TransactionStore;
import org.mengyun.tcctransaction.transaction.Transaction;
import org.mengyun.tcctransaction.utils.NetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


@RunWith(SpringRunner.class)
//@Ignore
public class RemotingStorageTest {

    private static final Logger logger = LoggerFactory.getLogger(RemotingStorageTest.class);
    private static TransactionSerializer transactionSerializer = new RegisterableKryoTransactionSerializer();
    private static TransactionStoreSerializer transactionStoreSerializer = new RegisterableKryoTransactionStoreSerializer();
    private static RemotingCommandSerializer remotingCommandSerializer = new RegisterableKryoRemotingCommandSerializer();
    private TccServer tccServer;

    @Before
    public void init() throws Exception {
        this.tccServer = new TccServer(new ServerConfig());
        tccServer.start();
    }

    @After
    public void close() throws Exception {
        if (tccServer != null) {
            tccServer.shutdown();
        }
    }

    @Test
    public void performance_test_remoting_transaction_storage_crud() throws InterruptedException {

        ClientConfig clientConfig = ClientConfig.DEFAULT;


        NettyRemotingClient remotingClient = new NettyRemotingClient(remotingCommandSerializer, clientConfig, new ServerAddressLoader() {
            @Override
            public InetSocketAddress selectOne(String key) {
                return NetUtils.toInetSocketAddress(clientConfig.getServerAddress());
            }

            @Override
            public List<InetSocketAddress> getAll(String key) {
                return Lists.newArrayList(NetUtils.toInetSocketAddress(clientConfig.getServerAddress()));
            }

            @Override
            public boolean isAvailableAddress(InetSocketAddress remoteAddress) {
                return NetUtils.toInetSocketAddress(clientConfig.getServerAddress()).equals(remoteAddress);
            }
        });

        remotingClient.start();

        RemotingTransactionStorage repository = new RemotingTransactionStorage(transactionStoreSerializer, clientConfig);
        repository.setRemotingClient(remotingClient);

        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2 + 1);

        int totalCount = 1000;
        AtomicInteger index = new AtomicInteger(0);

        long startTimeMillis = System.currentTimeMillis();

        doCreate(repository, executorService, totalCount, index);

        // restart the server side

        //doCreate(repository, executorService, totalCount, index);

        repository.close();

        logger.info("total run: " + index.get() + " with cost time:" + (System.currentTimeMillis() - startTimeMillis) / 1000.000);
    }


    private void doCreate(RemotingTransactionStorage repository, ExecutorService executorService, int totalCount, AtomicInteger index) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(totalCount);
        for (int i = 0; i < totalCount; i++) {
            int finalI = i;
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {

                        Transaction transaction = new Transaction(TransactionType.ROOT);

                        TransactionStore transactionStore = new TransactionStore();
                        transactionStore.setDomain("TCC:TEST");
                        transactionStore.setXid(transaction.getXid());
                        transactionStore.setStatusId(transaction.getStatus().getId());
                        transactionStore.setContent(transactionSerializer.serialize(transaction));
                        repository.create(transactionStore);
                        repository.update(transactionStore);
                        repository.findByXid(transactionStore.getDomain(), transactionStore.getXid());
                        repository.delete(transactionStore);
                        logger.debug("run done: " + index.incrementAndGet());
                    } catch (Exception e) {
                        logger.error("run failed.", e);
                    } finally {
                        countDownLatch.countDown();
                    }

                }
            });
        }
        countDownLatch.await();
    }
}
