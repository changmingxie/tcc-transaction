package org.mengyun.tcctransaction.dashboard.service.impl.tccserver;

import org.mengyun.tcctransaction.api.web.dto.ModifyCronDto;
import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.api.web.dto.TaskDto;
import org.mengyun.tcctransaction.dashboard.service.TaskService;
import org.mengyun.tcctransaction.dashboard.service.condition.TccServerStorageCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/5/30 11:51
 **/
@Conditional(TccServerStorageCondition.class)
@Service
public class TccServerTaskServiceImpl implements TaskService {

    @Autowired
    private TccServerFeignClient tccServerFeignClient;

    @Override
    public ResponseDto<List<TaskDto>> all() {
        return tccServerFeignClient.taskAll();
    }

    @Override
    public ResponseDto pause(String domain) {
        return tccServerFeignClient.taskPause(domain);
    }

    @Override
    public ResponseDto resume(String domain) {
        return tccServerFeignClient.taskResume(domain);
    }

    @Override
    public ResponseDto modifyCron(ModifyCronDto requestDto) {
        return tccServerFeignClient.taskModifyCron(requestDto);
    }
}
