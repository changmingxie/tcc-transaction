package org.mengyun.tcctransaction.dashboard.service;

import org.mengyun.tcctransaction.api.web.dto.ModifyCronDto;
import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.api.web.dto.TaskDto;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/5/30 10:08
 **/
public interface TaskService {

    /**
     * 所有任务
     *
     * @return
     */
    public ResponseDto<List<TaskDto>> all();

    /**
     * 暂停任务
     *
     * @param domain
     * @return
     */
    public ResponseDto pause(String domain);

    /**
     * 恢复任务
     *
     * @param domain
     * @return
     */
    public ResponseDto resume(String domain);

    /**
     * 修改任务cron表达式
     *
     * @param requestDto
     * @return
     */
    public ResponseDto modifyCron(@RequestBody ModifyCronDto requestDto);

}
