package org.mengyun.tcctransaction.dashboard.service.impl.tccserver;

import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.dashboard.service.DomainService;
import org.mengyun.tcctransaction.dashboard.service.condition.TccServerStorageCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/5/30 14:17
 **/
@Conditional(TccServerStorageCondition.class)
@Service
public class TccServerDomainServiceImpl implements DomainService {

    @Autowired
    private TccServerFeignClient tccServerFeignClient;

    @Override
    public ResponseDto<List<String>> all() {
        return tccServerFeignClient.domains();
    }
}
