package org.mengyun.tcctransaction.dashboard.controller;

import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.dashboard.model.LoginDto;
import org.mengyun.tcctransaction.dashboard.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author huabao.fang
 * @Date 2022/6/6 11:22
 **/
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    @ResponseBody
    public ResponseDto login(@RequestBody LoginDto request){
        String token = userService.login(request.getUsername(), request.getPassword());
        return ResponseDto.returnSuccess(token);
    }
}
