package org.mengyun.tcctransaction.dashboard.service.impl.local;

import org.mengyun.tcctransaction.TccClient;
import org.mengyun.tcctransaction.api.TransactionStatus;
import org.mengyun.tcctransaction.api.TransactionXid;
import org.mengyun.tcctransaction.api.web.dto.*;
import org.mengyun.tcctransaction.dashboard.service.TransactionService;
import org.mengyun.tcctransaction.dashboard.service.condition.LocalStorageCondition;
import org.mengyun.tcctransaction.storage.Page;
import org.mengyun.tcctransaction.storage.StorageRecoverable;
import org.mengyun.tcctransaction.storage.TransactionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/6/9 16:27
 **/
@Conditional(LocalStorageCondition.class)
@Service
public class LocalTransactionServiceImpl implements TransactionService {

    private static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private TccClient tccClient;

    /**
     * 事件分页查询
     * @param requestDto
     * @return
     */
    public ResponseDto<TransactionPageDto> list(TransactionPageRequestDto requestDto) {
        Page<TransactionStore> page = null;
        int total = 0;
        if(!StringUtils.isEmpty(requestDto.getXidString())){
            page = findByXid(requestDto);
            total = page.getData().size();
        }else{
            page = findPage(requestDto);
            total = findTotal(requestDto);
        }
        TransactionPageDto pageDto = new TransactionPageDto();
        pageDto.setNextOffset(page.getNextOffset());
        pageDto.setItems(toTransactionStoreDtoList(page.getData()));
        pageDto.setTotal(total);
        return ResponseDto.returnSuccess(pageDto);
    }

    @Override
    public ResponseDto<TransactionStoreDto> detail(TransactionDetailRequestDto requestDto) {
        return null;
    }

    private int findTotal(TransactionPageRequestDto requestDto){
        StorageRecoverable transactionStorage = (StorageRecoverable) tccClient.getTransactionStorage();
        return transactionStorage.findTotal(requestDto.getDomain(),requestDto.isMarkDeleted());
    }

    private Page<TransactionStore> findByXid(TransactionPageRequestDto requestDto){
        TransactionStore transactionStore = null;
        List<TransactionStore> list = new ArrayList<>();
        if(requestDto.isMarkDeleted()){
            transactionStore = tccClient.getTransactionStorage().findMarkDeletedByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        }else{
            transactionStore = tccClient.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        }
        if(transactionStore != null){
            list.add(transactionStore);
        }
        return new Page<>(null, list);
    }

    private Page<TransactionStore> findPage(TransactionPageRequestDto requestDto){
        StorageRecoverable transactionStorage = (StorageRecoverable) tccClient.getTransactionStorage();
        Integer pageSize = requestDto.getPageSize() <= 0 ? DEFAULT_PAGE_SIZE : requestDto.getPageSize();
        Page<TransactionStore> page = null;
        if(requestDto.isMarkDeleted()){
            page = transactionStorage.findAllDeletedSince(requestDto.getDomain(), new Date(), requestDto.getOffset(), pageSize);
        }else{
            page = transactionStorage.findAllUnmodifiedSince(requestDto.getDomain(), new Date(), requestDto.getOffset(), pageSize);
        }
        return page;
    }

    public ResponseDto confirm(TransactionOperateRequestDto requestDto){
        TransactionStore transactionStore = tccClient.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        transactionStore.setStatusId(TransactionStatus.CONFIRMING.getId());
        tccClient.getTransactionStorage().update(transactionStore);
        return ResponseDto.returnSuccess();
    }

    public ResponseDto cancel(TransactionOperateRequestDto requestDto){
        TransactionStore transactionStore = tccClient.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        transactionStore.setStatusId(TransactionStatus.CANCELLING.getId());
        tccClient.getTransactionStorage().update(transactionStore);
        return ResponseDto.returnSuccess();
    }

    public ResponseDto reset(TransactionOperateRequestDto requestDto){
        TransactionStore transactionStore = tccClient.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        transactionStore.setRetriedCount(0);
        tccClient.getTransactionStorage().update(transactionStore);
        return ResponseDto.returnSuccess();
    }

    /**
     * 软删除，事件标记为删除状态
     * @param requestDto
     */
    public ResponseDto markDeleted(TransactionOperateRequestDto requestDto){
        TransactionStore transactionStore = tccClient.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        tccClient.getTransactionStorage().markDeleted(transactionStore);
        return ResponseDto.returnSuccess();
    }

    /**
     * 软删除状态恢复为正常
     * @param requestDto
     */
    public ResponseDto restore(TransactionOperateRequestDto requestDto){
        // 由于软删除的key格式已经发生了变化 这里暂时 不做查询
        TransactionStore transactionStore = tccClient.getTransactionStorage().findMarkDeletedByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        tccClient.getTransactionStorage().restore(transactionStore);
        return ResponseDto.returnSuccess();
    }

    /**
     * 物理删除
     * @param requestDto
     */
    public ResponseDto delete(TransactionOperateRequestDto requestDto){
        TransactionStore transactionStore = tccClient.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        tccClient.getTransactionStorage().delete(transactionStore);
        return ResponseDto.returnSuccess();
    }


    private List<TransactionStoreDto> toTransactionStoreDtoList(List<TransactionStore> transactionStoreList){
        List<TransactionStoreDto> transactionStoreDtoList = new ArrayList<>(transactionStoreList.size());
        transactionStoreList.forEach(transactionStore -> {
            transactionStoreDtoList.add(toTransactionStoreDto(transactionStore));
        });
        return transactionStoreDtoList;
    }

    private TransactionStoreDto toTransactionStoreDto(TransactionStore transactionStore) {
        TransactionStoreDto transactionStoreDto = new TransactionStoreDto();
        transactionStoreDto.setDomain(transactionStore.getDomain());
        transactionStoreDto.setXid(transactionStore.getXid());
        transactionStoreDto.setRootXid(transactionStore.getRootXid());
        transactionStoreDto.setContent(transactionStore.getContent());
        transactionStoreDto.setCreateTime(transactionStore.getCreateTime());
        transactionStoreDto.setLastUpdateTime(transactionStore.getLastUpdateTime());
        transactionStoreDto.setVersion(transactionStore.getVersion());
        transactionStoreDto.setRetriedCount(transactionStore.getRetriedCount());
        transactionStoreDto.setStatusId(transactionStore.getStatusId());
        transactionStoreDto.setTransactionTypeId(transactionStore.getTransactionTypeId());

        transactionStoreDto.setXidString(transactionStore.getXid().toString());
        transactionStoreDto.setRootXidString(transactionStore.getRootXid().toString());
        return transactionStoreDto;
    }

}
