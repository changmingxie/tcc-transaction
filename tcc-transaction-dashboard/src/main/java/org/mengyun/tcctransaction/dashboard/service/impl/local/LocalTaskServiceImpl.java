package org.mengyun.tcctransaction.dashboard.service.impl.local;

import org.mengyun.tcctransaction.api.web.dto.ModifyCronDto;
import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.api.web.dto.TaskDto;
import org.mengyun.tcctransaction.api.web.enums.ResponseCodeEnum;
import org.mengyun.tcctransaction.api.web.exception.TransactionException;
import org.mengyun.tcctransaction.dashboard.service.TaskService;
import org.mengyun.tcctransaction.dashboard.service.condition.LocalStorageCondition;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/6/9 17:03
 **/
@Conditional(LocalStorageCondition.class)
@Service
public class LocalTaskServiceImpl implements TaskService {

    @Override
    public ResponseDto<List<TaskDto>> all() {
        throw new TransactionException(ResponseCodeEnum.TASK_OPERATE_NOT_SUPPORT);
    }

    @Override
    public ResponseDto pause(String domain) {
        throw new TransactionException(ResponseCodeEnum.TASK_OPERATE_NOT_SUPPORT);
    }

    @Override
    public ResponseDto resume(String domain) {
        throw new TransactionException(ResponseCodeEnum.TASK_OPERATE_NOT_SUPPORT);
    }

    @Override
    public ResponseDto modifyCron(ModifyCronDto requestDto) {
        throw new TransactionException(ResponseCodeEnum.TASK_OPERATE_NOT_SUPPORT);
    }

}
