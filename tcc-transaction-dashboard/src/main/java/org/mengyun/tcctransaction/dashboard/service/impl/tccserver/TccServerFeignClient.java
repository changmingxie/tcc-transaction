package org.mengyun.tcctransaction.dashboard.service.impl.tccserver;

import org.mengyun.tcctransaction.api.web.dto.ModifyCronDto;
import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.api.web.dto.TransactionOperateRequestDto;
import org.mengyun.tcctransaction.api.web.dto.TransactionPageRequestDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author huabao.fang
 * @Date 2022/5/30 10:10
 * ,url = "http://localhost:9998"
 **/
@FeignClient(value = "tcc-transaction-server")
public interface TccServerFeignClient {

    @RequestMapping("/domain/all")
    public ResponseDto domains();

    @RequestMapping("/task/all")
    @ResponseBody
    public ResponseDto taskAll();

    /**
     * 暂停任务
     *
     * @param domain
     * @return
     */
    @RequestMapping("/task/pause/{domain}")
    @ResponseBody
    public ResponseDto taskPause(@PathVariable("domain") String domain);

    /**
     * 恢复任务
     *
     * @param domain
     * @return
     */
    @RequestMapping("/task/resume/{domain}")
    @ResponseBody
    public ResponseDto taskResume(@PathVariable("domain") String domain);

    @RequestMapping("/task/modifyCron")
    @ResponseBody
    public ResponseDto taskModifyCron(@RequestBody ModifyCronDto requestDto);

    /**
     * 事件分页查询
     *
     * @param requestDto
     * @return
     */
    @RequestMapping("/transaction/list")
    @ResponseBody
    public ResponseDto transactionList(@RequestBody TransactionPageRequestDto requestDto);

    @RequestMapping("/transaction/confirm")
    @ResponseBody
    public ResponseDto transactionConfirm(@RequestBody TransactionOperateRequestDto requestDto);

    @RequestMapping("/transaction/cancel")
    @ResponseBody
    public ResponseDto transactionCancel(@RequestBody TransactionOperateRequestDto requestDto);

    @RequestMapping("/transaction/reset")
    @ResponseBody
    public ResponseDto transactionReset(@RequestBody TransactionOperateRequestDto requestDto);

    @RequestMapping("/transaction/markDeleted")
    @ResponseBody
    public ResponseDto transactionMarkDeleted(@RequestBody TransactionOperateRequestDto requestDto);

    @RequestMapping("/transaction/restore")
    @ResponseBody
    public ResponseDto transactionRestore(@RequestBody TransactionOperateRequestDto requestDto);

    @RequestMapping("/transaction/delete")
    @ResponseBody
    public ResponseDto transactionDelete(@RequestBody TransactionOperateRequestDto requestDto);

}
