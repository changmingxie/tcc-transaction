package org.mengyun.tcctransaction.dashboard.service;

import org.mengyun.tcctransaction.api.web.dto.ResponseDto;

import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/5/30 10:09
 **/
public interface DomainService {

    public ResponseDto<List<String>> all();

}
