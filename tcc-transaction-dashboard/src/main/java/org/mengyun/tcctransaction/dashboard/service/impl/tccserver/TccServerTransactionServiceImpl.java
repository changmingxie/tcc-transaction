package org.mengyun.tcctransaction.dashboard.service.impl.tccserver;

import org.mengyun.tcctransaction.api.web.dto.*;
import org.mengyun.tcctransaction.dashboard.service.TransactionService;
import org.mengyun.tcctransaction.dashboard.service.condition.TccServerStorageCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

/**
 * @Author huabao.fang
 * @Date 2022/5/30 14:19
 **/
@Conditional(TccServerStorageCondition.class)
@Service
public class TccServerTransactionServiceImpl implements TransactionService {

    @Autowired
    private TccServerFeignClient tccServerFeignClient;

    @Override
    public ResponseDto<TransactionPageDto> list(TransactionPageRequestDto requestDto) {
        return tccServerFeignClient.transactionList(requestDto);
    }

    @Override
    public ResponseDto<TransactionStoreDto> detail(TransactionDetailRequestDto requestDto) {
        throw new RuntimeException("not support");
    }

    @Override
    public ResponseDto confirm(TransactionOperateRequestDto requestDto) {
        return tccServerFeignClient.transactionConfirm(requestDto);
    }

    @Override
    public ResponseDto cancel(TransactionOperateRequestDto requestDto) {
        return tccServerFeignClient.transactionCancel(requestDto);
    }

    @Override
    public ResponseDto reset(TransactionOperateRequestDto requestDto) {
        return tccServerFeignClient.transactionReset(requestDto);
    }

    @Override
    public ResponseDto markDeleted(TransactionOperateRequestDto requestDto) {
        return tccServerFeignClient.transactionMarkDeleted(requestDto);
    }

    @Override
    public ResponseDto restore(TransactionOperateRequestDto requestDto) {
        return tccServerFeignClient.transactionRestore(requestDto);
    }

    @Override
    public ResponseDto delete(TransactionOperateRequestDto requestDto) {
        return tccServerFeignClient.transactionDelete(requestDto);
    }
}
