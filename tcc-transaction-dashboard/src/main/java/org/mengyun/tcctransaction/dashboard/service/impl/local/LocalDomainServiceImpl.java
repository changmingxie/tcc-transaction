package org.mengyun.tcctransaction.dashboard.service.impl.local;

import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.dashboard.service.DomainService;
import org.mengyun.tcctransaction.dashboard.service.condition.LocalStorageCondition;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/6/9 17:03
 **/
@Conditional(LocalStorageCondition.class)
@Service
public class LocalDomainServiceImpl implements DomainService {
    @Override
    public ResponseDto<List<String>> all() {
        return ResponseDto.returnSuccess(Arrays.asList("TCC", "TCC:HTTP:CAPITAL:", "TCC:HTTP:ORDER:", "TCC:HTTP:REDPACKET:"));
    }
}
