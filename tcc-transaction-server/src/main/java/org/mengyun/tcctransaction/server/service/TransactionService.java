package org.mengyun.tcctransaction.server.service;

import org.mengyun.tcctransaction.TccServer;
import org.mengyun.tcctransaction.api.TransactionStatus;
import org.mengyun.tcctransaction.api.TransactionXid;
import org.mengyun.tcctransaction.api.web.dto.TransactionOperateRequestDto;
import org.mengyun.tcctransaction.api.web.dto.TransactionPageDto;
import org.mengyun.tcctransaction.api.web.dto.TransactionPageRequestDto;
import org.mengyun.tcctransaction.api.web.dto.TransactionStoreDto;
import org.mengyun.tcctransaction.api.web.exception.TransactionException;
import org.mengyun.tcctransaction.storage.Page;
import org.mengyun.tcctransaction.storage.StorageRecoverable;
import org.mengyun.tcctransaction.storage.TransactionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.xa.Xid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @Author huabao.fang
 * @Date 2022/5/24 23:17
 **/
@Service
public class TransactionService {

    private static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private TccServer tccServer;

    /**
     * 事件分页查询
     *
     * @param requestDto
     * @return
     */
    public TransactionPageDto list(TransactionPageRequestDto requestDto) {
        Page<TransactionStore> page = null;
        int total = 0;
        if (!StringUtils.isEmpty(requestDto.getXidString())) {
            page = findByXid(requestDto);
            total = page.getData().size();
        } else {
            page = findPage(requestDto);
            total = findTotal(requestDto);
        }
        TransactionPageDto pageDto = new TransactionPageDto();
        pageDto.setNextOffset(page.getNextOffset());
        pageDto.setItems(toTransactionStoreDtoList(page.getData()));
        pageDto.setTotal(total);
        return pageDto;
    }

    private int findTotal(TransactionPageRequestDto requestDto) {
        StorageRecoverable transactionStorage = (StorageRecoverable) tccServer.getTransactionStorage();
        return transactionStorage.findTotal(requestDto.getDomain(), requestDto.isMarkDeleted());
    }

    private Page<TransactionStore> findByXid(TransactionPageRequestDto requestDto) {
        TransactionStore transactionStore = null;
        List<TransactionStore> list = new ArrayList<>();
        if (requestDto.isMarkDeleted()) {
            transactionStore = tccServer.getTransactionStorage().findMarkDeletedByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        } else {
            transactionStore = tccServer.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        }
        if (transactionStore != null) {
            list.add(transactionStore);
        }
        return new Page<>(null, list);
    }

    private Page<TransactionStore> findPage(TransactionPageRequestDto requestDto) {
        StorageRecoverable transactionStorage = (StorageRecoverable) tccServer.getTransactionStorage();
        Integer pageSize = requestDto.getPageSize() <= 0 ? DEFAULT_PAGE_SIZE : requestDto.getPageSize();
        Page<TransactionStore> page = null;
        if (requestDto.isMarkDeleted()) {
            page = transactionStorage.findAllDeletedSince(requestDto.getDomain(), new Date(), requestDto.getOffset(), pageSize);
        } else {
            page = transactionStorage.findAllUnmodifiedSince(requestDto.getDomain(), new Date(), requestDto.getOffset(), pageSize);
        }
        return page;
    }

    public void confirm(TransactionOperateRequestDto requestDto) {
        TransactionStore transactionStore = tccServer.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        transactionStore.setStatusId(TransactionStatus.CONFIRMING.getId());
        tccServer.getTransactionStorage().update(transactionStore);
    }

    public void cancel(TransactionOperateRequestDto requestDto) {
        TransactionStore transactionStore = tccServer.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        transactionStore.setStatusId(TransactionStatus.CANCELLING.getId());
        tccServer.getTransactionStorage().update(transactionStore);
    }

    public void reset(TransactionOperateRequestDto requestDto) {
        TransactionStore transactionStore = tccServer.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        transactionStore.setRetriedCount(0);
        tccServer.getTransactionStorage().update(transactionStore);
    }

    /**
     * 软删除，事件标记为删除状态
     *
     * @param requestDto
     */
    public void markDeleted(TransactionOperateRequestDto requestDto) {
        TransactionStore transactionStore = tccServer.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        tccServer.getTransactionStorage().markDeleted(transactionStore);
    }

    /**
     * 软删除状态恢复为正常
     *
     * @param requestDto
     */
    public void restore(TransactionOperateRequestDto requestDto) {
        // 由于软删除的key格式已经发生了变化 这里暂时 不做查询
        TransactionStore transactionStore = tccServer.getTransactionStorage().findMarkDeletedByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        tccServer.getTransactionStorage().restore(transactionStore);
    }

    /**
     * 物理删除
     *
     * @param requestDto
     */
    public void delete(TransactionOperateRequestDto requestDto) {
        TransactionStore transactionStore = tccServer.getTransactionStorage().findByXid(requestDto.getDomain(), TransactionXid.toXid(requestDto.getXidString()));
        tccServer.getTransactionStorage().delete(transactionStore);
    }


    private List<TransactionStoreDto> toTransactionStoreDtoList(List<TransactionStore> transactionStoreList) {
        List<TransactionStoreDto> transactionStoreDtoList = new ArrayList<>(transactionStoreList.size());
        transactionStoreList.forEach(transactionStore -> {
            transactionStoreDtoList.add(toTransactionStoreDto(transactionStore));
        });
        return transactionStoreDtoList;
    }

    private TransactionStoreDto toTransactionStoreDto(TransactionStore transactionStore) {
        TransactionStoreDto transactionStoreDto = new TransactionStoreDto();
        transactionStoreDto.setDomain(transactionStore.getDomain());
        transactionStoreDto.setXid(transactionStore.getXid());
        transactionStoreDto.setRootXid(transactionStore.getRootXid());
        transactionStoreDto.setContent(transactionStore.getContent());
        transactionStoreDto.setCreateTime(transactionStore.getCreateTime());
        transactionStoreDto.setLastUpdateTime(transactionStore.getLastUpdateTime());
        transactionStoreDto.setVersion(transactionStore.getVersion());
        transactionStoreDto.setRetriedCount(transactionStore.getRetriedCount());
        transactionStoreDto.setStatusId(transactionStore.getStatusId());
        transactionStoreDto.setTransactionTypeId(transactionStore.getTransactionTypeId());

        transactionStoreDto.setXidString(transactionStore.getXid().toString());
        transactionStoreDto.setRootXidString(transactionStore.getRootXid().toString());
        return transactionStoreDto;
    }


}
