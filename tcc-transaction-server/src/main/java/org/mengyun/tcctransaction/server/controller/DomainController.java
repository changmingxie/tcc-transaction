package org.mengyun.tcctransaction.server.controller;

import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.server.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author huabao.fang
 * @Date 2022/5/23 14:53
 **/
@RestController
@RequestMapping("domain")
public class DomainController {
    @Autowired
    private TaskService taskService;

    @RequestMapping("/all")
    @ResponseBody
    public ResponseDto all() {
        return ResponseDto.returnSuccess(taskService.getAllDomain());
    }
}
