package org.mengyun.tcctransaction.server.controller;

import org.mengyun.tcctransaction.api.web.dto.ModifyCronDto;
import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.server.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author huabao.fang
 * @Date 2022/5/19 15:21
 * 任务管理
 **/
@RestController
@RequestMapping("task")
public class TaskController {

    private Logger logger = LoggerFactory.getLogger(TaskController.class.getSimpleName());

    @Autowired
    private TaskService taskService;

    @RequestMapping("/all")
    @ResponseBody
    public ResponseDto all() {
        return ResponseDto.returnSuccess(taskService.allTask());
    }

    /**
     * 暂停任务
     *
     * @param domain
     * @return
     */
    @RequestMapping("/pause/{domain}")
    @ResponseBody
    public ResponseDto pause(@PathVariable("domain") String domain) {
        taskService.pause(domain);
        return ResponseDto.returnSuccess();
    }

    /**
     * 恢复任务
     *
     * @param domain
     * @return
     */
    @RequestMapping("/resume/{domain}")
    @ResponseBody
    public ResponseDto resume(@PathVariable("domain") String domain) {
        taskService.resume(domain);
        return ResponseDto.returnSuccess();
    }

    @RequestMapping("/modifyCron")
    @ResponseBody
    public ResponseDto modifyCron(@RequestBody ModifyCronDto requestDto) {
        taskService.modifyCron(requestDto);
        return ResponseDto.returnSuccess();
    }


}
