package org.mengyun.tcctransaction.server.service;

import org.mengyun.tcctransaction.TccServer;
import org.mengyun.tcctransaction.api.web.dto.ModifyCronDto;
import org.mengyun.tcctransaction.api.web.dto.TaskDto;
import org.mengyun.tcctransaction.utils.StringUtils;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author huabao.fang
 * @Date 2022/5/24 19:55
 **/
@Service
public class TaskService {

    private Logger logger = LoggerFactory.getLogger(TaskService.class.getSimpleName());

    @Autowired
    private TccServer tccServer;

    public Set<String> getAllDomain() {
        Map<String, JobKey> jobKeyMap = buildDomainJobKeyMap();
        return jobKeyMap.keySet();
    }

    public Collection<TaskDto> allTask() {
        Map<String, TaskDto> domainTaskDtoMap = buildDomainTaskDtoMap();
        return domainTaskDtoMap.values();
    }

    public void pause(String domain) {
        try {
            JobKey jobKey = selectJobKey(domain);
            tccServer.getScheduler().getScheduler().pauseJob(jobKey);
            logger.info("domain:{} task paused", domain);
        } catch (SchedulerException e) {
            logger.error("pasuse job for domain:{} error", domain, e);
            throw new RuntimeException("pasuse job for domain error", e);
        }
    }

    public void resume(String domain) {
        try {
            JobKey jobKey = selectJobKey(domain);
            tccServer.getScheduler().getScheduler().resumeJob(jobKey);
            logger.info("domain:{} task resumed", domain);
        } catch (SchedulerException e) {
            logger.error("resume job for domain:{} error", domain, e);
            throw new RuntimeException("resume job for domain error", e);
        }
    }

    public void modifyCron(ModifyCronDto requestDto) {
        Map<String, TriggerKey> triggerKeyMap = this.buildDomainTriggerKeyMap();
        Scheduler scheduler = tccServer.getScheduler().getScheduler();
        TriggerKey triggerKey = triggerKeyMap.get(requestDto.getDomain());
        try {
            CronTrigger currentCronTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            String currentCron = currentCronTrigger.getCronExpression();
            if (StringUtils.isNotEmpty(requestDto.getCronExpression())
                    && !requestDto.getCronExpression().equals(currentCron)) {
                updateTrigger(scheduler, triggerKey, requestDto.getCronExpression());
            }
            logger.info("domain:{} update cron from {} to {} success", requestDto.getDomain(), currentCron, requestDto.getCronExpression());
        } catch (SchedulerException e) {
            logger.error("modifyCron error", e);
            throw new RuntimeException("modifyCron error", e);
        }
    }

    private void updateTrigger(Scheduler scheduler, TriggerKey triggerKey, String cron) throws SchedulerException {

        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(triggerKey.getName())
                .withSchedule(CronScheduleBuilder.cronSchedule(cron)
                        .withMisfireHandlingInstructionDoNothing()).build();

        scheduler.rescheduleJob(triggerKey, cronTrigger);
    }

    private JobKey selectJobKey(String domain) {
        Map<String, JobKey> jobKeyMap = buildDomainJobKeyMap();
        return jobKeyMap.get(domain);
    }

    private Map<String, TaskDto> buildDomainTaskDtoMap() {
        Map<String, TaskDto> domainTaskDtoMap = new HashMap<>();
        Map<String, JobKey> domainJobKeyMap = buildDomainJobKeyMap();
        Map<String, TriggerKey> domainTriggerKeyMap = buildDomainTriggerKeyMap();
        Scheduler scheduler = tccServer.getScheduler().getScheduler();
        domainJobKeyMap.keySet().forEach(domain -> {
            try {
                JobKey jobKey = domainJobKeyMap.get(domain);
                TriggerKey triggerKey = domainTriggerKeyMap.get(domain);
                // TriggerState { NONE, NORMAL, PAUSED, COMPLETE, ERROR, BLOCKED }
                Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
                CronTrigger cronTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
                domainTaskDtoMap.put(domain, new TaskDto(domain, jobKey.getGroup(), jobKey.getName(), triggerState.name(), cronTrigger.getCronExpression()));
            } catch (Exception e) {
                logger.error("build domain taskDto map error", e);
                throw new RuntimeException("build domain taskDto map error", e);
            }
        });
        return domainTaskDtoMap;
    }

    /**
     * 构造domain与jobKey之间的映射
     *
     * @return
     */
    private Map<String, JobKey> buildDomainJobKeyMap() {

        Map<String, JobKey> domainJobKeyMap = new HashMap<>();
        Set<JobKey> jobKeySet = null;
        try {
            jobKeySet = tccServer.getScheduler().getScheduler().getJobKeys(GroupMatcher.anyGroup());
            jobKeySet.forEach(jobKey -> {
                String[] arr = jobKey.getName().split("_");
                if (arr.length > 1) {
                    String domain = arr[1];
                    if (domainJobKeyMap.containsKey(domain)) {
                        logger.warn("domain:{} repeated at jobKey:{}", domain, jobKey.toString());
                    }
                    domainJobKeyMap.put(domain, jobKey);
                }
            });
        } catch (SchedulerException e) {
            logger.error("build domain job key map error", e);
            throw new RuntimeException("build domain job key map error", e);
        }
        return domainJobKeyMap;
    }

    /**
     * 构造domain与triggerKey的映射
     *
     * @return
     */
    private Map<String, TriggerKey> buildDomainTriggerKeyMap() {
        Map<String, TriggerKey> domainTriggerKeyMap = new HashMap<>();
        Set<TriggerKey> triggerKeySet = null;
        try {
            triggerKeySet = tccServer.getScheduler().getScheduler().getTriggerKeys(GroupMatcher.anyGroup());
            triggerKeySet.forEach(triggerKey -> {
                String[] arr = triggerKey.getName().split("_");
                if (arr.length > 1) {
                    String domain = arr[1];
                    if (domainTriggerKeyMap.containsKey(domain)) {
                        logger.warn("domain:{} repeated at triggerKey:{}", domain, triggerKey.toString());
                    }
                    domainTriggerKeyMap.put(domain, triggerKey);
                }
            });
        } catch (SchedulerException e) {
            logger.error("build domain triggerKey map error", e);
            throw new RuntimeException("build domain triggerKey map error", e);
        }
        return domainTriggerKeyMap;
    }
}
