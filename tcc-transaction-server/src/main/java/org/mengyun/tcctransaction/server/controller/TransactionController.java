package org.mengyun.tcctransaction.server.controller;

import org.mengyun.tcctransaction.api.web.dto.ResponseDto;
import org.mengyun.tcctransaction.api.web.dto.TransactionOperateRequestDto;
import org.mengyun.tcctransaction.api.web.dto.TransactionPageRequestDto;
import org.mengyun.tcctransaction.server.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author huabao.fang
 * @Date 2022/5/19 12:19
 * 事件管理
 **/
@RestController
@RequestMapping("transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    /**
     * 事件分页查询
     *
     * @param requestDto
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public ResponseDto list(@RequestBody TransactionPageRequestDto requestDto) {
        return ResponseDto.returnSuccess(transactionService.list(requestDto));
    }

    @RequestMapping("/confirm")
    @ResponseBody
    public ResponseDto confirm(@RequestBody TransactionOperateRequestDto requestDto) {
        transactionService.confirm(requestDto);
        return ResponseDto.returnSuccess();
    }

    @RequestMapping("/cancel")
    @ResponseBody
    public ResponseDto cancel(@RequestBody TransactionOperateRequestDto requestDto) {
        transactionService.cancel(requestDto);
        return ResponseDto.returnSuccess();
    }

    @RequestMapping("/reset")
    @ResponseBody
    public ResponseDto reset(@RequestBody TransactionOperateRequestDto requestDto) {
        transactionService.reset(requestDto);
        return ResponseDto.returnSuccess();
    }

    @RequestMapping("/markDeleted")
    @ResponseBody
    public ResponseDto markDeleted(@RequestBody TransactionOperateRequestDto requestDto) {
        transactionService.markDeleted(requestDto);
        return ResponseDto.returnSuccess();
    }

    @RequestMapping("/restore")
    @ResponseBody
    public ResponseDto restore(@RequestBody TransactionOperateRequestDto requestDto) {
        transactionService.restore(requestDto);
        return ResponseDto.returnSuccess();
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResponseDto delete(@RequestBody TransactionOperateRequestDto requestDto) {
        transactionService.delete(requestDto);
        return ResponseDto.returnSuccess();
    }


}
