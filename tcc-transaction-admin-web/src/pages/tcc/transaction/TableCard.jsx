import React, {useEffect, useState} from 'react';
import {Button, Card, Col, message, Modal, Row, Space, Table, Popconfirm, Tag} from 'antd';
import {columns} from '../../../common/constants';
import {useSelector} from 'react-redux';
import * as api from '../../../common/api';
import ReactJson from "react-json-view";
import {RightOutlined, LeftOutlined, ExclamationCircleOutlined} from '@ant-design/icons';



const TableCard = (props) => {
  const {activeTabKey} = props;
  const deleted = activeTabKey === 'deletedKeys';
  const {currentDomain: domain, xidString, refresh} = useSelector(({domain}) => domain);
  const [datasource, setdatasource] = useState([]); //列表数据
  const [total, setTotal] = useState(0); //总数
  const [loadedOffSetStack, setLoadedOffSetStack] = useState([]);
  const [nextOffset, setNextOffset] = useState(null); //下一页
  const [loadingStatus, setloadingStatus] = useState(false); //加载数据
  const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const onSelectChange = (selectedRowKeys, selectedRows) => {
    console.log('selectedRows changed: ', selectedRows);
    setSelectedRows(selectedRows);
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const initPagination = {
    offset: null,
    pageSize: 10,
    forward: true
  };
  const pagination = {
    ...initPagination
  };

  const pageInit = ()=>{
    setdatasource([])
    setSelectedRows([])
    setSelectedRowKeys([])
    getListData(initPagination);
  }
  //获取列表数据
  const getListData = (pagination) => {
    if(!domain){
      return ;
    }
    setloadingStatus(true);
    let data = {
      domain,
      xidString,
      offset: pagination.offset,
      pageSize: pagination.pageSize,
      deleted,
    };
    api
      .getManageList(data)
      .then((res) => {
        console.log("getManageList",res)
        setloadingStatus(false);
        if(res.items&&res.items.length>0){
          setdatasource(res.items);
          setTotal(res.total);
          if(pagination.forward){
            setNextOffset(res.nextOffset)
            loadedOffSetStack.push(data.offset)
          }else{
            setNextOffset(data.offset)
          }
        }else{
          message.success('没有数据了😁');
        }
      })
      .catch((res) => {
        setloadingStatus(false);
        message.error('服务异常，请稍后再试');
      });
  };

  const reloadHandler = (promises) => {
    promises
      .then((resultList) => {// TODO 需要进行异常测试
        let failCount = 0;
        let sucCount = resultList.length;
        if(failCount == 0){
          message.success('批量操作成功');
        }else{
          message.warn("批量操作：成功数("+sucCount+"),失败数("+failCount+")")
        }
        if(sucCount>0){
          pageInit()
        }
      })
      .catch(e => {
        message.error(e.message || '服务异常，请稍后再试');
      });
  };
  const next = (pagination)=>{
    console.log("next")
    pageChange(true)
  };
  const last = (pagination)=>{
    console.log("last")
    pageChange(false)
  };
  useEffect(() => {
    pageInit();
  }, [refresh,activeTabKey]);



  const pageChange = (forward)=>{
    let curPagination = {}
    if(forward){
      curPagination={
        offset: nextOffset,
        pageSize: pagination.pageSize,
        forward: forward
      }
    }else{
      curPagination={
        offset: loadedOffSetStack.pop(),
        pageSize: pagination.pageSize,
        forward: forward
      }
    }
    setSelectedRowKeys([])
    getListData(curPagination);
  }

  const batchHandle = (handle)=>{
    if(!selectedRows||selectedRows.length<=0){
      message.warn("请至少选择一条事件");
      return;
    }
    let resultPromiseList = new Array();
    for(let i = 0;i<selectedRows.length;i++){
      resultPromiseList.push(
        handle({
          ...selectedRows[i]
        }));
    }
    reloadHandler(Promise.all(resultPromiseList));
  };

  return(
    <Card>
      {
        deleted ?
          <Space size="small" style={{float: "left", marginBottom: '1rem'}}>
            <Popconfirm
              title="是否执行"
              onConfirm={() => batchHandle(api.restore)}
              okText="是"
              cancelText="否">
              <Button
                size="small"
                type="primary"
                danger>恢复</Button>
            </Popconfirm>
          </Space>
          :
          <Space size="small" style={{float: "left", marginBottom: '1rem'}}>
            <Popconfirm
              title="是否执行"
              onConfirm={() => batchHandle(api.confirm)}
              okText="是"
              cancelText="否">
              <Button
                size="small"
                type="primary"
                style={{backgroundColor: '#faad14', borderColor: '#faad14'}}>确认</Button>
            </Popconfirm>
            <Popconfirm
              title="是否执行"
              onConfirm={() => batchHandle(api.cancel)}
              okText="是"
              cancelText="否">
              <Button
                size="small"
                type="primary"
                style={{backgroundColor: '#faad14', borderColor: '#faad14'}}>取消</Button>
            </Popconfirm>
            <Popconfirm
              title="是否执行"
              onConfirm={() => batchHandle(api.reset)}
              okText="是"
              cancelText="否">
              <Button
                size="small"
                type="primary"
                style={{backgroundColor: '#faad14', borderColor: '#faad14'}}>重置</Button>
            </Popconfirm>
            <Popconfirm
              title="是否执行"
              onConfirm={() => batchHandle(api.remove)}
              okText="是"
              cancelText="否">
              <Button
                size="small"
                type="primary"
                danger>删除</Button>
            </Popconfirm>
          </Space>
      }

      <Space size="small" style={{float: "right"}}>
        <Tag color="#f50">总数:{total}</Tag>
        {
          loadedOffSetStack.length <= 0 ?
            <Button disabled size="small" type="primary" onClick={() => last(pagination)}
                    icon={<LeftOutlined/>}></Button>
            :
            <Button size="small" type="primary" onClick={() => last(pagination)} icon={<LeftOutlined/>}></Button>
        }
        <Button className="button" size="small" type="primary" onClick={() => next(pagination)}
                icon={<RightOutlined/>}></Button>
      </Space>
      <Table
        rowKey={record => record.xidString}
        rowSelection={rowSelection}
        columns={columns.concat({
          title: '事件详情',
          key: 'detail',
          fixed: 'right',
          width: 80,
          render: (text, record) => (
            <Space>
              <Button
                className="button"
                size="small"
                type="primary"
                onClick={() => {
                  Modal.info({
                    content: <ReactJson src={JSON.parse('{"message":"待开发"}')}/>,
                    width: '90%',
                  });
                }}
              >
                详情
              </Button>
            </Space>
          ),
        })
        }
        dataSource={datasource}
        size="small"
        bordered
        loading={loadingStatus}
        pagination={false}
        scroll={{x: 1500, y: 800}}
      />
    </Card>
  );
};

export default TableCard;
