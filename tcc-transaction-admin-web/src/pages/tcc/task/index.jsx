import React, { useState, useEffect } from 'react';
import {getAllTask, taskModifyCron, taskPause, taskResume} from '../../../common/api';
import {Table, Button, Row, Col, Space, message,Modal, Form, Input} from 'antd';


const Page = () => {
  const [taskList, setTaskList] = useState([]);
  const [loadingStatus, setloadingStatus] = useState(false); //加载数据
  const [modifyModalVisible, setModifyModalVisible] = useState(false);
  const [waitModifyTask, setWaitModifyTask] = useState({});
  const [form] = Form.useForm();  //form实例

  useEffect(() => {
    reLoadAllTaskList();
  }, []);
  useEffect(() => {
    form.resetFields();
  }, [waitModifyTask]);

  const reLoadAllTaskList = ()=>{
    setloadingStatus(true);
    getAllTask().then(data => {
      setloadingStatus(false);
      setTaskList(data);
    }) .catch((res) => {
      setloadingStatus(false);
      message.error('服务异常，请稍后再试');
    });
  }
  const columns = [
    {
      title: 'domain',
      dataIndex: 'domain',
      key: 'domain',
    },
    {
      title: 'job组',
      dataIndex: 'jobGroup',
      key: 'jobGroup',
    },
    {
      title: 'job名称',
      dataIndex: 'jobName',
      key: 'jobName',
    },
    {
      title: 'cron表达式',
      dataIndex: 'cronExpression',
      key: 'cronExpression',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'operation',
      key: 'operation',
      render: (text, record) => (
        <Space>
          {
            record.status === 'NORMAL'?
              <Button
                size="small"
                type="primary"
                danger
                onClick={() => {
                  console.log("pause",record)
                  taskPause(record.domain).then(resp=>{
                    reLoadAllTaskList()
                  })
                }} >暂停</Button>
              :<></>
          }
          {
            record.status !== 'NORMAL'?
              <Button
                size="small"
                type="primary"
                style={{ backgroundColor:'#faad14',borderColor:'#faad14'}}
                onClick={() => {
                  taskResume(record.domain).then(resp=>{
                    reLoadAllTaskList()
                  })
                }}>恢复</Button>
              :<></>
          }
          <Button
            size="small"
            type="primary"
            onClick={()=>{
              showModifyModal(record)
            }}>
            修改
          </Button>
        </Space>
      ),
    }
  ];
  const showModifyModal = (record) => {
    setWaitModifyTask({
      ...record
    })
    setModifyModalVisible(true);
  };

  const handleCancel = () => {
    setModifyModalVisible(false);
  };


  const onFinish = async () => {
    const values = await form.validateFields();
    taskModifyCron(values).then(res=>{
      setModifyModalVisible(false);
      reLoadAllTaskList();
    }) .catch((res) => {
      message.error('服务异常，请稍后再试');
    });
  };
  return (
    <div>

      <Row style={{padding: '12px'}}>
        <Col span={23}>
          &nbsp;
        </Col>
        <Col span={1}>
          <Button type="primary" onClick={() => reLoadAllTaskList()}>
            刷新
          </Button>
        </Col>
      </Row>
      <Table rowKey={record=>record.domain}
             dataSource={taskList}
             columns={columns}
             pagination={false}
             loading={loadingStatus}/>
      <Modal title="修改" visible={modifyModalVisible}
             getContainer={false}
             forceRender
             closable={false}
             footer={[
               <Button key="cancel" type="primary" onClick={handleCancel}>取消</Button>,
               <Button key="submit" type="primary" onClick={onFinish}>确认</Button>
             ]}
      >
        <Form
          name="basic"
          form= {form}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            label="domain"
            name="domain"
            initialValue={waitModifyTask.domain}>
            <Input disabled/>
          </Form.Item>
          <Form.Item
            label="cron表达式"
            name="cronExpression"
            initialValue={waitModifyTask.cronExpression}
            rules={[{ required: true, message: '请输入cronExpression' }]}>
            <Input/>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default Page;
