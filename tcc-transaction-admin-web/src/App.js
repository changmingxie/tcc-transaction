import React from 'react';
import {Provider} from 'react-redux';
import {HashRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import 'antd/dist/antd.css';
import './App.css';

import Login from './pages/tcc/login/index';
import Welcome from './pages/tcc/welcome/index';
import Transaction from './pages/tcc/transaction/index';
import Task from "./pages/tcc/task/index";

import store from './store';
import TccLayout from "./layout/TccLayout";

function App(props) {
  return (
    <Provider store={store}>
      {/*/tcc-transaction-dashboard/page*/}
      <Router>
        <Switch>
          {/*<Route key="home" path="/" render={() => <Redirect to="/welcome" />}></Route>*/}
          <Route key="login" path="/login" component={Login}></Route>
          <TccLayout routeList={
            <>
              <Route path="/welcome" component={Welcome}></Route>
              <Route path="/transaction" component={Transaction}></Route>
              <Route path="/task" component={Task}></Route>
            </>
          }>
          </TccLayout>
          <Redirect to="/welcome" from="/"/>
        </Switch>
      </Router>
    </Provider>
  );

}

export default App;
