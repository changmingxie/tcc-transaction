package org.mengyun.tcctransaction.spring.xml.configuration;


import org.mengyun.tcctransaction.ClientConfig;
import org.mengyun.tcctransaction.spring.ConfigurableCoordinatorAspect;
import org.mengyun.tcctransaction.spring.ConfigurableTransactionAspect;
import org.mengyun.tcctransaction.spring.SpringTccClient;
import org.mengyun.tcctransaction.spring.factory.SpringBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(value = "org.mengyun.tcctransaction", excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {XmlTccTransactionConfiguration.class})})
public class AnnotationTccTransactionConfiguration {

    @Autowired(required = false)
    private ClientConfig clientConfig;

    @Bean("springBeanFactory")
    public SpringBeanFactory getSpringBeanFactory() {
        return new SpringBeanFactory();
    }


    @Bean("configurableTransactionAspect")
    public ConfigurableTransactionAspect getConfigurableTransactionAspect() {
        ConfigurableTransactionAspect aspect = new ConfigurableTransactionAspect();
        return aspect;
    }

    @Bean("configurableCoordinatorAspect")
    public ConfigurableCoordinatorAspect getConfigurableCoordinatorAspect() {
        ConfigurableCoordinatorAspect aspect = new ConfigurableCoordinatorAspect();
        return aspect;
    }

    @Bean
    @DependsOn({"springBeanFactory"})
    public SpringTccClient getTccClient() {
        SpringTccClient tccClient = new SpringTccClient(clientConfig);
        return tccClient;
    }

//    @Bean
//    @Conditional(FeignInterceptorCondition.class)
//    public FactoryBean tccRequestFeignConfiguration() {
//        return new FactoryBean() {
//            @Override
//            public Object getObject() throws Exception {
//                return Class.forName(ClassNameConstants.TCC_REQUEST_INTERCEPTOR_CLASS_NAME).newInstance();
//            }
//
//            @Override
//            public Class<?> getObjectType() {
//                try {
//                    return Class.forName(ClassNameConstants.TCC_REQUEST_INTERCEPTOR_CLASS_NAME);
//                } catch (ClassNotFoundException e) {
//                    return null;
//                }
//            }
//        };
//    }
}
