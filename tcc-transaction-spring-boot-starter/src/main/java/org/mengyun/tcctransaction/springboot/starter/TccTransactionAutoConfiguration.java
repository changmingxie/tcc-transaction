package org.mengyun.tcctransaction.springboot.starter;

import org.mengyun.tcctransaction.ClientConfig;
import org.mengyun.tcctransaction.discovery.registry.RegistryConfig;
import org.mengyun.tcctransaction.properties.CommonProperties;
import org.mengyun.tcctransaction.properties.RecoveryProperties;
import org.mengyun.tcctransaction.properties.RegistryProperties;
import org.mengyun.tcctransaction.properties.remoting.NettyClientProperties;
import org.mengyun.tcctransaction.properties.store.StoreProperties;
import org.mengyun.tcctransaction.recovery.RecoveryConfig;
import org.mengyun.tcctransaction.remoting.netty.NettyClientConfig;
import org.mengyun.tcctransaction.spring.annotation.EnableTccTransaction;
import org.mengyun.tcctransaction.storage.StoreConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @author Nervose.Wu
 * @date 2022/5/26 11:48
 */
@EnableTccTransaction
@EnableConfigurationProperties
public class TccTransactionAutoConfiguration {

    @Bean
    @ConfigurationProperties("spring.tcc")
    public CommonProperties commonConfigProperties() {
        return new CommonProperties();
    }

    @Bean
    @ConfigurationProperties("spring.tcc.remoting")
    public NettyClientProperties nettyClientProperties() {
        return new NettyClientProperties();
    }

    @Bean
    @ConfigurationProperties("spring.tcc.storage")
    public StoreProperties storeProperties() {
        return new StoreProperties();
    }

    @Bean
    @ConfigurationProperties("spring.tcc.registry")
    public RegistryProperties registryProperties() {
        return new RegistryProperties();
    }

    @Bean
    @ConfigurationProperties("spring.tcc.recovery")
    public RecoveryProperties recoveryProperties() {
        return new RecoveryProperties();
    }

    @Bean
    public ClientConfig clientConfig(@Autowired CommonProperties commonProperties,
                                     @Autowired RegistryConfig registryConfig,
                                     @Autowired StoreConfig storeConfig,
                                     @Autowired RecoveryConfig recoveryConfig,
                                     @Autowired NettyClientConfig nettyClientConfig) {
        return new ClientConfig(commonProperties, storeConfig, recoveryConfig, nettyClientConfig, registryConfig);
    }
}
