package org.mengyun.tcctransaction.processor;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.mengyun.tcctransaction.constants.RemotingServiceCode;
import org.mengyun.tcctransaction.recovery.RecoveryExecutor;
import org.mengyun.tcctransaction.recovery.RecoveryScheduler;
import org.mengyun.tcctransaction.remoting.protocol.RemotingCommand;
import org.mengyun.tcctransaction.remoting.protocol.RemotingCommandCode;
import org.mengyun.tcctransaction.serializer.TransactionStoreSerializer;
import org.mengyun.tcctransaction.storage.StorageRecoverable;
import org.mengyun.tcctransaction.storage.TransactionStorage;
import org.mengyun.tcctransaction.storage.TransactionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ServerRecoveryExecutor implements RecoveryExecutor {

    static final Logger logger = LoggerFactory.getLogger(ServerRecoveryExecutor.class.getSimpleName());
    private TransactionStoreSerializer serializer;
    private RecoveryScheduler scheduler;
    private TransactionStorage transactionStorage;

    private ConcurrentMap<String, ChannelGroup> channelGroup = new ConcurrentHashMap<>();

    public ServerRecoveryExecutor(RecoveryScheduler scheduler, TransactionStoreSerializer transactionStoreSerializer, TransactionStorage transactionStorage) {
        this.serializer = transactionStoreSerializer;
        this.scheduler = scheduler;
        this.transactionStorage = transactionStorage;
    }

    public void registerDomain(String domain) {
        if(transactionStorage instanceof StorageRecoverable){
            ((StorageRecoverable)transactionStorage).registerDomain(domain);
        }else{
            logger.warn("transactionStorage:{} not StorageRecoverable, do not regist domain",transactionStorage.getClass().getSimpleName());
        }
    }

    public void registerChannel(String domain, Channel channel) {

        channelGroup.computeIfAbsent(domain, key -> {
            ChannelGroup domainChannelGroup = new DefaultChannelGroup(key, GlobalEventExecutor.INSTANCE);
            return domainChannelGroup;
        });

        channelGroup.get(domain).add(channel);
    }

    public void rollback(TransactionStore transactionStore) {
        doRecover(RemotingServiceCode.RECOVER_ROLLBACK, transactionStore);
    }

    public void commit(TransactionStore transactionStore) {
        doRecover(RemotingServiceCode.RECOVER_COMMIT, transactionStore);
    }

    public void registerRecoveryTask(String domain) {
        scheduler.scheduleJob(domain);
    }

    private void doRecover(int serviceCode, TransactionStore transactionStore) {
        RemotingCommand remotingCommand = RemotingCommand.createCommand(RemotingCommandCode.SERVICE_REQ, null);
        remotingCommand.setServiceCode(serviceCode);
        remotingCommand.setBody(serializer.serialize(transactionStore));

        Set<Channel> channels = channelGroup.get(transactionStore.getDomain());

        if (channels != null && channels.size() > 0) {
            for (Channel channel : channels) {
                try {
                    channel.writeAndFlush(remotingCommand).addListener(new ChannelFutureListener() {
                        @Override
                        public void operationComplete(ChannelFuture channelFuture) throws Exception {
                            if (!channelFuture.isSuccess()) {
                                logger.warn("send recovery command with service code " + serviceCode + " to channel <" + channel.remoteAddress() + "> failed.");
                            }
                        }
                    });
                    break;
                } catch (Exception e) {
                    logger.warn("cannot recover", e);
                }
            }
        } else {
            logger.debug(String.format("no available client channels for domain<%s> to recovery", transactionStore.getDomain()));
        }
    }
}
