package org.mengyun.tcctransaction.processor;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import org.mengyun.tcctransaction.constants.RemotingServiceCode;
import org.mengyun.tcctransaction.recovery.RecoveryExecutor;
import org.mengyun.tcctransaction.remoting.RequestProcessor;
import org.mengyun.tcctransaction.remoting.protocol.RemotingCommand;
import org.mengyun.tcctransaction.serializer.ObjectSerializer;
import org.mengyun.tcctransaction.serializer.TransactionStoreSerializer;
import org.mengyun.tcctransaction.storage.TransactionStorage;
import org.mengyun.tcctransaction.storage.TransactionStore;

import javax.transaction.xa.Xid;

public class ServerRequestProcessor implements RequestProcessor<ChannelHandlerContext> {

    private TransactionStorage transactionStorage;

    private ServerRecoveryExecutor recoveryExecutor;

    private TransactionStoreSerializer serializer;

    public ServerRequestProcessor(TransactionStoreSerializer serializer, RecoveryExecutor recoveryExecutor, TransactionStorage transactionStorage) {
        this.serializer = serializer;
        this.transactionStorage = transactionStorage;
        this.recoveryExecutor = (ServerRecoveryExecutor) recoveryExecutor;
    }

    @Override
    public RemotingCommand processRequest(ChannelHandlerContext ctx, RemotingCommand request) {

        if (request.getServiceCode() == RemotingServiceCode.FIND) {
            return doReadProcess(ctx, request);
        } else if (request.getServiceCode() == RemotingServiceCode.REGISTER) {
            return doRegister(ctx, request);
        } else {
            return doWriteProcess(ctx, request);
        }
    }

    private RemotingCommand doRegister(ChannelHandlerContext ctx, RemotingCommand request) {

        //store the domain and the channel map
        String domain = new String(request.getBody());
        Channel channel = ctx.channel();
        recoveryExecutor.registerDomain(domain);
        recoveryExecutor.registerChannel(domain, channel);
        recoveryExecutor.registerRecoveryTask(domain);

        RemotingCommand remotingCommand = RemotingCommand.createServiceResponseCommand(null);
        remotingCommand.setBody(new byte[]{Integer.valueOf(1).byteValue()});
        return remotingCommand;
    }

    private RemotingCommand doReadProcess(ChannelHandlerContext ctx, RemotingCommand request) {

        byte[] content = request.getBody();
        byte domainBytesLength = content[0];
        byte[] domainBytes = new byte[domainBytesLength];
        byte[] xidBytes = new byte[content.length - domainBytes.length - 1];

        System.arraycopy(content, 1, domainBytes, 0, domainBytesLength);
        System.arraycopy(content, 1 + domainBytes.length, xidBytes, 0, content.length - domainBytes.length - 1);

        Xid xid = (Xid) ((ObjectSerializer) serializer).deserialize(xidBytes);
        String domain = new String(domainBytes);

        TransactionStore transaction = transactionStorage.findByXid(domain, xid);

        RemotingCommand remotingCommand = RemotingCommand.createServiceResponseCommand(null);
        remotingCommand.setBody(serializer.serialize(transaction));
        return remotingCommand;
    }

    private RemotingCommand doWriteProcess(ChannelHandlerContext ctx, RemotingCommand request) {

        TransactionStore transaction = serializer.deserialize(request.getBody());

        int result = -1;
        switch (request.getServiceCode()) {
            case RemotingServiceCode.CREATE:
                result = transactionStorage.create(transaction);
                break;
            case RemotingServiceCode.UPDATE:
                result = transactionStorage.update(transaction);
                break;
            case RemotingServiceCode.DELETE:
                result = transactionStorage.delete(transaction);
                break;
        }

        if (result > 0) {
            //store the domain and the channel map
            String domain = transaction.getDomain();
            Channel channel = ctx.channel();
            recoveryExecutor.registerChannel(domain, channel);
        }

        RemotingCommand remotingCommand = RemotingCommand.createServiceResponseCommand(null);
        remotingCommand.setBody(new byte[]{Integer.valueOf(result).byteValue()});
        return remotingCommand;
    }
}
