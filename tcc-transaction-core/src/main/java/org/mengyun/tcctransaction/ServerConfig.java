package org.mengyun.tcctransaction;

import org.mengyun.tcctransaction.discovery.registry.RegistryConfig;
import org.mengyun.tcctransaction.properties.CommonProperties;
import org.mengyun.tcctransaction.properties.remoting.NettyServerProperties;
import org.mengyun.tcctransaction.recovery.RecoveryConfig;
import org.mengyun.tcctransaction.remoting.netty.NettyServerConfig;
import org.mengyun.tcctransaction.storage.StoreConfig;

public class ServerConfig extends AbstractConfig implements NettyServerConfig, RecoveryConfig, StoreConfig, RegistryConfig {

    public final static ServerConfig DEFAULT = new ServerConfig();

    private NettyServerConfig nettyServerConfig = new NettyServerProperties();

    public ServerConfig() {
    }

    public ServerConfig(CommonProperties commonProperties, StoreConfig storeConfig, RecoveryConfig recoveryConfig, NettyServerConfig nettyServerConfig, RegistryConfig registryConfig) {
        super(commonProperties, storeConfig, recoveryConfig, nettyServerConfig, registryConfig);
        if (nettyServerConfig != null) {
            this.nettyServerConfig = nettyServerConfig;
        }
    }

    @Override
    public int getListenPort() {
        return nettyServerConfig.getListenPort();
    }

    @Override
    public int getChannelIdleTimeoutSeconds() {
        return nettyServerConfig.getChannelIdleTimeoutSeconds();
    }

    public void setNettyServerConfig(NettyServerConfig nettyServerConfig) {
        this.nettyServerConfig = nettyServerConfig;
        setNettyConfig(nettyServerConfig);
    }
}
