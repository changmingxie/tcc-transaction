package org.mengyun.tcctransaction.repository;

public enum StoreMode {
    DISTRIBUTED,
    CENTRAL
}
