package org.mengyun.tcctransaction.storage;

import java.util.Date;

public interface StorageRecoverable {

    Page<TransactionStore> findAllUnmodifiedSince(String domain, Date date, String offset, int pageSize);

    Page<TransactionStore> findAllDeletedSince(String domain, Date date, String offset, int pageSize);

    // rename count
    int findTotal(String domain, boolean isMarkDeleted);

    // register domain
    void registerDomain(String domain);
    //
}
