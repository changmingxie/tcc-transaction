package org.mengyun.tcctransaction.storage;

import org.apache.commons.lang3.StringUtils;
import org.mengyun.tcctransaction.serializer.TransactionStoreSerializer;
import org.mengyun.tcctransaction.utils.CollectionUtils;

import javax.sql.DataSource;
import javax.transaction.xa.Xid;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by changmingxie on 10/30/15.
 */
public class JdbcTransactionStorage extends AbstractTransactionStorage implements StorageRecoverable {

    private static final int MARK_DELETED_YES = 1;
    private static final int MARK_DELETED_NO = 0;

    private String tbSuffix;
    private DataSource dataSource;

    public JdbcTransactionStorage(TransactionStoreSerializer serializer, StoreConfig storeConfig) {
        super(serializer, storeConfig);
        this.tbSuffix = storeConfig.getTbSuffix();
        this.dataSource = storeConfig.getDataSource();
    }

    public String getTbSuffix() {
        return tbSuffix;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected int doCreate(TransactionStore transactionStore) {

        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            connection = this.getConnection();

            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO " + getTableName() +
                    "(GLOBAL_TX_ID,BRANCH_QUALIFIER,TRANSACTION_TYPE,CONTENT,STATUS,RETRIED_COUNT,CREATE_TIME,LAST_UPDATE_TIME,VERSION");
            builder.append(StringUtils.isNotEmpty(transactionStore.getDomain()) ? ",DOMAIN) VALUES (?,?,?,?,?,?,?,?,?,?)" : ") VALUES (?,?,?,?,?,?,?,?,?)");

            stmt = connection.prepareStatement(builder.toString());

            stmt.setBytes(1, transactionStore.getXid().getGlobalTransactionId());
            stmt.setBytes(2, transactionStore.getXid().getBranchQualifier());
            stmt.setInt(3, transactionStore.getTransactionTypeId());
            stmt.setBytes(4, serializer.serialize(transactionStore));
            stmt.setInt(5, transactionStore.getStatusId());
            stmt.setInt(6, transactionStore.getRetriedCount());
            stmt.setTimestamp(7, new Timestamp(transactionStore.getCreateTime().getTime()));
            stmt.setTimestamp(8, new Timestamp(transactionStore.getLastUpdateTime().getTime()));
            stmt.setLong(9, transactionStore.getVersion());

            if (StringUtils.isNotEmpty(transactionStore.getDomain())) {
                stmt.setString(10, transactionStore.getDomain());
            }

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new TransactionIOException(e);
        } finally {
            closeStatement(stmt);
            this.releaseConnection(connection);
        }
    }

    @Override
    protected int doUpdate(TransactionStore transactionStore) {
        Connection connection = null;
        PreparedStatement stmt = null;

        Date lastUpdateTime = transactionStore.getLastUpdateTime();
        long currentVersion = transactionStore.getVersion();

        transactionStore.setLastUpdateTime(new Date());
        transactionStore.setVersion(transactionStore.getVersion() + 1);

        try {
            connection = this.getConnection();

            StringBuilder builder = new StringBuilder();
            builder.append("UPDATE " + getTableName() + " SET " +
                    "CONTENT = ?,STATUS = ?,LAST_UPDATE_TIME = ?, RETRIED_COUNT = ?,VERSION = VERSION+1 WHERE GLOBAL_TX_ID = ? AND BRANCH_QUALIFIER = ? AND VERSION = ?");

            builder.append(StringUtils.isNotEmpty(transactionStore.getDomain()) ? " AND DOMAIN = ?" : "");

            stmt = connection.prepareStatement(builder.toString());

            stmt.setBytes(1, serializer.serialize(transactionStore));
            stmt.setInt(2, transactionStore.getStatusId());
            stmt.setTimestamp(3, new Timestamp(transactionStore.getLastUpdateTime().getTime()));

            stmt.setInt(4, transactionStore.getRetriedCount());
            stmt.setBytes(5, transactionStore.getXid().getGlobalTransactionId());
            stmt.setBytes(6, transactionStore.getXid().getBranchQualifier());
            stmt.setLong(7, currentVersion);

            if (StringUtils.isNotEmpty(transactionStore.getDomain())) {
                stmt.setString(8, transactionStore.getDomain());
            }

            int result = stmt.executeUpdate();

            return result;

        } catch (Throwable e) {
            transactionStore.setLastUpdateTime(lastUpdateTime);
            transactionStore.setVersion(currentVersion);
            throw new TransactionIOException(e);
        } finally {
            closeStatement(stmt);
            this.releaseConnection(connection);
        }
    }

    @Override
    protected int doDelete(TransactionStore transactionStore) {
        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            connection = this.getConnection();

            StringBuilder builder = new StringBuilder();
            builder.append("DELETE FROM " + getTableName() +
                    " WHERE GLOBAL_TX_ID = ? AND BRANCH_QUALIFIER = ?");

            builder.append(StringUtils.isNotEmpty(transactionStore.getDomain()) ? " AND DOMAIN = ?" : "");

            stmt = connection.prepareStatement(builder.toString());

            stmt.setBytes(1, transactionStore.getXid().getGlobalTransactionId());
            stmt.setBytes(2, transactionStore.getXid().getBranchQualifier());

            if (StringUtils.isNotEmpty(transactionStore.getDomain())) {
                stmt.setString(3, transactionStore.getDomain());
            }

            return stmt.executeUpdate();

        } catch (SQLException e) {
            throw new TransactionIOException(e);
        } finally {
            closeStatement(stmt);
            this.releaseConnection(connection);
        }
    }

    @Override
    protected int doMarkDeleted(TransactionStore transactionStore) {
        return doMarkDeleted(transactionStore, true);
    }

    @Override
    protected int doRestore(TransactionStore transactionStore) {
        return doMarkDeleted(transactionStore, false);
    }

    private int doMarkDeleted(TransactionStore transactionStore, boolean isMarkDeleted) {
        Connection connection = null;
        PreparedStatement stmt = null;

        Date lastUpdateTime = transactionStore.getLastUpdateTime();
        long currentVersion = transactionStore.getVersion();

        transactionStore.setLastUpdateTime(new Date());
        transactionStore.setVersion(transactionStore.getVersion() + 1);

        try {
            connection = this.getConnection();

            StringBuilder builder = new StringBuilder();
            builder.append("UPDATE " + getTableName() + " SET " +
                    "IS_DELETE = ?,LAST_UPDATE_TIME = ?, VERSION = VERSION+1 WHERE GLOBAL_TX_ID = ? AND BRANCH_QUALIFIER = ? AND VERSION = ?");

            builder.append(StringUtils.isNotEmpty(transactionStore.getDomain()) ? " AND DOMAIN = ?" : "");

            stmt = connection.prepareStatement(builder.toString());

            stmt.setInt(1, isMarkDeleted ? MARK_DELETED_YES : MARK_DELETED_NO);
            stmt.setTimestamp(2, new Timestamp(transactionStore.getLastUpdateTime().getTime()));

            stmt.setBytes(3, transactionStore.getXid().getGlobalTransactionId());
            stmt.setBytes(4, transactionStore.getXid().getBranchQualifier());
            stmt.setLong(5, currentVersion);

            if (StringUtils.isNotEmpty(transactionStore.getDomain())) {
                stmt.setString(6, transactionStore.getDomain());
            }

            int result = stmt.executeUpdate();

            return result;

        } catch (Throwable e) {
            transactionStore.setLastUpdateTime(lastUpdateTime);
            transactionStore.setVersion(currentVersion);
            throw new TransactionIOException(e);
        } finally {
            closeStatement(stmt);
            this.releaseConnection(connection);
        }
    }

    @Override
    protected TransactionStore doFindOne(String domain, Xid xid, boolean isMarkDeleted) {
        return doFind(domain, getTableName(), xid, isMarkDeleted);
    }

    @Override
    public Page<TransactionStore> findAllUnmodifiedSince(String domain, Date date, String offset, int pageSize) {
        return pageList(domain, date, offset, pageSize, false);
    }

    @Override
    public Page<TransactionStore> findAllDeletedSince(String domain, Date date, String offset, int pageSize) {
        return pageList(domain, date, offset, pageSize, true);
    }

    @Override
    public int findTotal(String domain, boolean isMarkDeleted) {

        Connection connection = null;
        PreparedStatement stmt = null;

        try {

            connection = this.getConnection();

            StringBuilder builder = new StringBuilder();
            builder.append("SELECT COUNT(1)");
            builder.append("  FROM " + getTableName() + " WHERE  DOMAIN = ? AND IS_DELETE = ?");
            stmt = connection.prepareStatement(builder.toString());
            stmt.setString(1, domain);
            stmt.setInt(2, isMarkDeleted ? MARK_DELETED_YES : MARK_DELETED_NO);

            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return 0;
        } catch (Throwable e) {
            throw new TransactionIOException(e);
        } finally {
            closeStatement(stmt);
            this.releaseConnection(connection);
        }
    }

    @Override
    public void registerDomain(String domain) {

    }

    private Page<TransactionStore> pageList(String domain, Date date, String offset, int pageSize, boolean isMarkDeleted) {

        List<TransactionStore> transactions = new ArrayList<TransactionStore>();

        Connection connection = null;
        PreparedStatement stmt = null;

        int currentOffset = StringUtils.isEmpty(offset) ? 0 : Integer.valueOf(offset);

        try {
            connection = this.getConnection();

            StringBuilder builder = new StringBuilder();

            builder.append("SELECT GLOBAL_TX_ID, BRANCH_QUALIFIER, CONTENT,STATUS,TRANSACTION_TYPE,CREATE_TIME,LAST_UPDATE_TIME,RETRIED_COUNT,VERSION");
            builder.append(StringUtils.isNotEmpty(domain) ? ",DOMAIN" : "");
            builder.append("  FROM " + getTableName() + " WHERE LAST_UPDATE_TIME < ?");
            builder.append(" AND IS_DELETE = ?");
            builder.append(StringUtils.isNotEmpty(domain) ? " AND DOMAIN = ?" : "");
            builder.append(" ORDER BY TRANSACTION_ID ASC");
            builder.append(String.format(" LIMIT %s, %d", currentOffset, pageSize));

            stmt = connection.prepareStatement(builder.toString());

            stmt.setTimestamp(1, new Timestamp(date.getTime()));
            stmt.setInt(2, isMarkDeleted ? MARK_DELETED_YES : MARK_DELETED_NO);

            if (StringUtils.isNotEmpty(domain)) {
                stmt.setString(3, domain);
            }

            ResultSet resultSet = stmt.executeQuery();

            this.constructTransactions(resultSet, transactions);
        } catch (Throwable e) {
            throw new TransactionIOException(e);
        } finally {
            closeStatement(stmt);
            this.releaseConnection(connection);
        }

        return new Page<TransactionStore>(String.valueOf(currentOffset + transactions.size()), transactions);
    }

    private TransactionStore doFind(String domain, String tableName, Xid xid, boolean isMarkDeleted) {

        List<TransactionStore> transactions = doFinds(domain, tableName, Arrays.asList(xid), isMarkDeleted);

        if (!CollectionUtils.isEmpty(transactions)) {
            return transactions.get(0);
        }
        return null;
    }

    private List<TransactionStore> doFinds(String domain, String tableName, List<Xid> xids, boolean isMarkDeleted) {

        List<TransactionStore> transactions = new ArrayList<TransactionStore>();

        if (CollectionUtils.isEmpty(xids)) {
            return transactions;
        }

        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            connection = this.getConnection();

            StringBuilder builder = new StringBuilder();
            builder.append("SELECT GLOBAL_TX_ID, BRANCH_QUALIFIER, CONTENT,STATUS,TRANSACTION_TYPE,CREATE_TIME,LAST_UPDATE_TIME,RETRIED_COUNT,VERSION,IS_DELETE");
            builder.append(StringUtils.isNotEmpty(domain) ? ",DOMAIN" : "");
            builder.append("  FROM " + getTableName() + " WHERE");

            if (!CollectionUtils.isEmpty(xids)) {
                for (Xid xid : xids) {
                    builder.append(" ( GLOBAL_TX_ID = ? AND BRANCH_QUALIFIER = ? ) OR");
                }

                builder.delete(builder.length() - 2, builder.length());
            }

            builder.append(StringUtils.isNotEmpty(domain) ? " AND DOMAIN = ?" : "");
            builder.append(" AND IS_DELETE = ?");

            stmt = connection.prepareStatement(builder.toString());

            int i = 0;

            for (Xid xid : xids) {
                stmt.setBytes(++i, xid.getGlobalTransactionId());
                stmt.setBytes(++i, xid.getBranchQualifier());
            }

            if (StringUtils.isNotEmpty(domain)) {
                stmt.setString(++i, domain);
            }

            stmt.setInt(++i, isMarkDeleted ? MARK_DELETED_YES : MARK_DELETED_NO);
            ResultSet resultSet = stmt.executeQuery();

            this.constructTransactions(resultSet, transactions);
        } catch (Throwable e) {
            throw new TransactionIOException(e);
        } finally {
            closeStatement(stmt);
            this.releaseConnection(connection);
        }

        return transactions;
    }

    private void constructTransactions(ResultSet resultSet, List<TransactionStore> transactions) throws SQLException {
        while (resultSet.next()) {
            byte[] transactionBytes = resultSet.getBytes(3);
            TransactionStore transactionStore = (TransactionStore) serializer.deserialize(transactionBytes);
            transactionStore.setStatusId(resultSet.getInt(4));
            transactionStore.setLastUpdateTime(resultSet.getDate(7));
            transactionStore.setVersion(resultSet.getLong(9));
            transactionStore.setRetriedCount(resultSet.getInt(8));
            transactions.add(transactionStore);
        }
    }

    private Connection getConnection() {
        try {
            return this.dataSource.getConnection();
        } catch (SQLException e) {
            throw new TransactionIOException(e);
        }
    }

    private void releaseConnection(Connection con) {
        try {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        } catch (SQLException e) {
            throw new TransactionIOException(e);
        }
    }

    private void closeStatement(Statement stmt) {
        try {
            if (stmt != null && !stmt.isClosed()) {
                stmt.close();
            }
        } catch (Exception ex) {
            throw new TransactionIOException(ex);
        }
    }

    private String getTableName() {
        return StringUtils.isNotEmpty(tbSuffix) ? "TCC_TRANSACTION_" + tbSuffix : "TCC_TRANSACTION";
    }
}
