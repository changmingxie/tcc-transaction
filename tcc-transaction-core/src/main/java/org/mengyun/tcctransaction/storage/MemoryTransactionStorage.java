package org.mengyun.tcctransaction.storage;

import com.google.common.collect.Lists;
import org.mengyun.tcctransaction.serializer.TransactionStoreSerializer;
import org.mengyun.tcctransaction.storage.helper.ShardHolder;
import org.mengyun.tcctransaction.storage.helper.ShardOffset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.xa.Xid;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryTransactionStorage extends AbstractKVTransactionStorage<Map<Xid, TransactionStore>> {

    static final Logger log = LoggerFactory.getLogger(MemoryTransactionStorage.class.getSimpleName());

    private Map<Xid, TransactionStore> db = new ConcurrentHashMap<>();

    public MemoryTransactionStorage(TransactionStoreSerializer serializer, StoreConfig storeConfig) {
        super(serializer, storeConfig);
    }

    @Override
    protected List<TransactionStore> findTransactionsFromOneShard(String domain, Map<Xid, TransactionStore> shard, Set keys) {

        List<TransactionStore> list = new ArrayList<TransactionStore>();

        for (Object key : keys) {
            list.add(shard.get(key));
        }

        return list;
    }

    @Override
    Page<Xid> findKeysFromOneShard(String domain, Map<Xid, TransactionStore> shard, String currentCursor, int maxFindCount, boolean isDeletedQuery) {

        Page<Xid> page = new Page<>();
        Iterator<Xid> iterator = shard.keySet().iterator();
        int iteratorIndex = 0;
        int currentIndex = Integer.valueOf(currentCursor);

        int count = 0;

        while (iterator.hasNext() && count < maxFindCount) {

            if (iteratorIndex < currentIndex) {
                iteratorIndex++;
                continue;
            }

            page.getData().add(iterator.next());
            count++;

            iteratorIndex++;
        }
        String nextCursor = ShardOffset.SCAN_INIT_CURSOR;

        if (iterator.hasNext() && count == maxFindCount) {
            nextCursor = String.valueOf(iteratorIndex);
        }

        page.setAttachment(nextCursor);

        return page;
    }

    @Override
    int count(String domain, Map<Xid, TransactionStore> shard, boolean isMarkDeleted) {
        return 0;
    }

    @Override
    protected ShardHolder<Map<Xid, TransactionStore>> getShardHolder() {
        return new ShardHolder<Map<Xid, TransactionStore>>() {
            @Override
            public List<Map<Xid, TransactionStore>> getAllShards() {
                return Lists.newArrayList(db);
            }

            @Override
            public void close() throws IOException {
            }
        };
    }

    @Override
    protected int doCreate(TransactionStore transactionStore) {
        trace("create", transactionStore);
        db.put(transactionStore.getXid(), transactionStore);
        return 1;
    }

    @Override
    protected int doUpdate(TransactionStore transactionStore) {
        trace("update", transactionStore);
        TransactionStore foundTransaction = doFindOne(transactionStore.getDomain(), transactionStore.getXid(), false);
        if (foundTransaction.getVersion() != transactionStore.getVersion()) {
            return 0;
        }

        transactionStore.setVersion(transactionStore.getVersion() + 1);
        transactionStore.setLastUpdateTime(new Date());
        db.put(transactionStore.getXid(), transactionStore);
        return 1;
    }

    @Override
    protected int doDelete(TransactionStore transactionStore) {
        trace("delete", transactionStore);
        db.remove(transactionStore.getXid());
        return 1;
    }

    @Override
    protected int doMarkDeleted(TransactionStore transactionStore) {
        // FIXME
        throw new RuntimeException("doMarkDeleted not support at memory");
    }

    @Override
    protected int doRestore(TransactionStore transactionStore) {
        // FIXME
        throw new RuntimeException("doRestore not support at memory");
    }

    @Override
    protected TransactionStore doFindOne(String domain, Xid xid, boolean isMarkDeleted) {
        if (isMarkDeleted) {
            // FIXME
            throw new RuntimeException("doRestore not support at memory");
        }
        return db.get(xid);
    }

    private void trace(String action, TransactionStore transactionStore) {

        StringBuilder sb = new StringBuilder();

        sb.append("MemoryStoreTransactionRepository." + action + "\r\n");
        sb.append("transactionStore xid:" + transactionStore.getXid() + "; status:" + transactionStore.getStatusId() + "\r\n");
        sb.append("content:" + new String(transactionStore.getContent()));

        log.debug(sb.toString());
    }

    @Override
    public void registerDomain(String domain) {

    }
}
