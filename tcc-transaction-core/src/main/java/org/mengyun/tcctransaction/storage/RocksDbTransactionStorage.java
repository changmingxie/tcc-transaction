package org.mengyun.tcctransaction.storage;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.mengyun.tcctransaction.exception.SystemException;
import org.mengyun.tcctransaction.serializer.TransactionStoreSerializer;
import org.mengyun.tcctransaction.storage.helper.ShardHolder;
import org.mengyun.tcctransaction.storage.helper.ShardOffset;
import org.rocksdb.Options;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksDBException;
import org.rocksdb.RocksIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.xa.Xid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class RocksDbTransactionStorage extends AbstractKVTransactionStorage<RocksDB> {
    static final Logger log = LoggerFactory.getLogger(RocksDbTransactionStorage.class.getSimpleName());

    static {
        RocksDB.loadLibrary();
    }

    private Options options;

    private RocksDB db;

    private String location = "/var/log/";

    private volatile boolean initialized = false;

    public RocksDbTransactionStorage(TransactionStoreSerializer serializer, StoreConfig storeConfig) {
        super(serializer, storeConfig);
        this.location = storeConfig.getLocation();
        init();
    }

    public String getLocation() {
        return location;
    }

    public void init() {

        if (!initialized) {

            synchronized (this) {

                if (!initialized) {
                    if (options == null)
                        // the Options class contains a set of configurable DB options
                        // that determines the behaviour of the database.
                        options = new Options().setCreateIfMissing(true).setKeepLogFileNum(1l);
                    String filePath = this.location;

                    try {
                        db = RocksDB.open(options, filePath);
                    } catch (RocksDBException e) {
                        throw new SystemException("open rocksdb failed.", e);
                    }

                    initialized = true;
                }
            }
        }
    }

    @Override
    protected int doCreate(TransactionStore transactionStore) {

        try {
            db.put(transactionStore.getXid().toString().getBytes(), getSerializer().serialize(transactionStore));
            return 1;
        } catch (RocksDBException e) {
            throw new TransactionIOException(e);
        }
    }

    @Override
    protected int doUpdate(TransactionStore transactionStore) {

        try {

            TransactionStore foundTransaction = doFindOne(transactionStore.getDomain(), transactionStore.getXid(),false);
            if (foundTransaction.getVersion() != transactionStore.getVersion()) {
                return 0;
            }

            transactionStore.setVersion(transactionStore.getVersion() + 1);
            transactionStore.setLastUpdateTime(new Date());
            db.put(transactionStore.getXid().toString().getBytes(), getSerializer().serialize(transactionStore));
            return 1;
        } catch (RocksDBException e) {
            throw new TransactionIOException(e);
        }
    }

    @Override
    protected int doDelete(TransactionStore transactionStore) {

        try {
            db.delete(transactionStore.getXid().toString().getBytes());
        } catch (RocksDBException e) {
            throw new TransactionIOException(e);
        }
        return 1;
    }

    @Override
    protected int doMarkDeleted(TransactionStore transactionStore) {
        // FIXME
        throw new RuntimeException("doMarkDeleted not support at rocksdb");
    }

    @Override
    protected int doRestore(TransactionStore transactionStore) {
        // FIXME
        throw new RuntimeException("doRestore not support at rocksdb");
    }

    @Override
    protected TransactionStore doFindOne(String domain, Xid xid, boolean isMarkDeleted) {
        if(isMarkDeleted){
            // FIXME
            throw new RuntimeException("doFindOne for markDeleted not support at rocksdb");
        }
        return doFind(db, xid);
    }

    @Override
    Page<byte[]> findKeysFromOneShard(String domain, RocksDB shard, String currentCursor, int maxFindCount, boolean isDeletedQuery) {

        Page<byte[]> page = new Page<>();

        try (final RocksIterator iterator = shard.newIterator()) {

            if (ShardOffset.SCAN_INIT_CURSOR.equals(currentCursor)) {
                iterator.seekToFirst();
            } else {
                iterator.seek(currentCursor.getBytes());
            }

            int count = 0;

            while (iterator.isValid() && count < maxFindCount) {

                page.getData().add(iterator.key());

                count++;
                iterator.next();
            }

            String nextCursor = ShardOffset.SCAN_INIT_CURSOR;
            if (iterator.isValid() && count == maxFindCount) {
                nextCursor = new String(iterator.key());
            }

            page.setAttachment(nextCursor);
        }
        return page;
    }

    @Override
    int count(String domain, RocksDB shard, boolean isMarkDeleted) {
        return 0;
    }


    @Override
    protected List<TransactionStore> findTransactionsFromOneShard(String domain, RocksDB shard, Set keys) {
        List<TransactionStore> list = null;

        List<byte[]> allValues = null;

        try {
            allValues = shard.multiGetAsList(Lists.newLinkedList(keys));
        } catch (RocksDBException e) {
            log.error("get transactionStore data from RocksDb failed.");
        }

        list = new ArrayList<TransactionStore>();

        for (byte[] value : allValues) {

            if (value != null) {
                list.add(getSerializer().deserialize(value));
            }
        }

        return list;
    }


    @Override
    protected ShardHolder<RocksDB> getShardHolder() {
        return new ShardHolder<RocksDB>() {
            @Override
            public List<RocksDB> getAllShards() {
                return Lists.newArrayList(db);
            }

            @Override
            public void close() throws IOException {

            }
        };
    }

    public void close() {

        if (db != null) {
            db.close();
        }

        if (options != null) {
            options.close();
        }
    }

    private TransactionStore doFind(RocksDB db, Xid xid) {

        try {
            byte[] values = db.get(xid.toString().getBytes());
            if (ArrayUtils.isNotEmpty(values)) {
                return getSerializer().deserialize(values);
            }
        } catch (RocksDBException e) {
            throw new TransactionIOException(e);
        }
        return null;
    }

    @Override
    public void registerDomain(String domain) {

    }
}
