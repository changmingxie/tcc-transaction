package org.mengyun.tcctransaction.storage;

import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ShardedJedisPool;

import javax.sql.DataSource;

public interface StoreConfig {

    StorageType getStorageType();

    String getDomain();

    String getTransactionStorageClass();

    long getRequestTimeoutMillis();

    String getLocation();

    JedisPool getJedisPool();

    ShardedJedisPool getShardedJedisPool();

    JedisCluster getJedisCluster();

    String getServerAddress();

    String getTbSuffix();

    DataSource getDataSource();

    int getKryoPoolSize();
}
