package org.mengyun.tcctransaction;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.mengyun.tcctransaction.constants.RemotingServiceCode;
import org.mengyun.tcctransaction.discovery.loadbalance.LoadBalanceFactory;
import org.mengyun.tcctransaction.discovery.loadbalance.LoadBalanceServcie;
import org.mengyun.tcctransaction.discovery.registry.RegistryFactory;
import org.mengyun.tcctransaction.discovery.registry.RegistryService;
import org.mengyun.tcctransaction.processor.ClientRecoveryExecutor;
import org.mengyun.tcctransaction.processor.ClientRecoveryProcessor;
import org.mengyun.tcctransaction.recovery.RecoveryExecutor;
import org.mengyun.tcctransaction.recovery.RecoveryScheduler;
import org.mengyun.tcctransaction.recovery.TransactionStoreRecovery;
import org.mengyun.tcctransaction.remoting.RemotingClient;
import org.mengyun.tcctransaction.remoting.RequestProcessor;
import org.mengyun.tcctransaction.remoting.netty.NettyRemotingClient;
import org.mengyun.tcctransaction.remoting.netty.ServerAddressLoader;
import org.mengyun.tcctransaction.remoting.protocol.RemotingCommand;
import org.mengyun.tcctransaction.remoting.protocol.RemotingCommandCode;
import org.mengyun.tcctransaction.repository.DefaultTransactionRepository;
import org.mengyun.tcctransaction.repository.StoreMode;
import org.mengyun.tcctransaction.repository.TransactionRepository;
import org.mengyun.tcctransaction.serializer.*;
import org.mengyun.tcctransaction.storage.RemotingTransactionStorage;
import org.mengyun.tcctransaction.storage.StorageType;
import org.mengyun.tcctransaction.storage.TransactionStorage;
import org.mengyun.tcctransaction.storage.TransactionStorageFactory;
import org.mengyun.tcctransaction.transaction.TransactionManager;
import org.mengyun.tcctransaction.utils.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by changming.xie on 11/25/17.
 */
public class TccClient implements TccService {

    static final Logger logger = LoggerFactory.getLogger(TccClient.class.getSimpleName());
    //attribute inject
    private ClientConfig clientConfig = ClientConfig.DEFAULT;
    //attribute inject
    private TransactionStorage transactionStorage;
    //attribute inject
    private RecoveryExecutor recoveryExecutor;
    //attribute inject
    private RequestProcessor requestProcessor;

    private TransactionStoreRecovery transactionStoreRecovery;

    private ExecutorService requestProcessExecutor;

    private TransactionStoreSerializer transactionStoreSerializer;

    private RecoveryScheduler scheduler;

    private RemotingClient remotingClient;

    //attribute inject
    private TransactionManager transactionManager;
    //attribute inject
    private TransactionRepository transactionRepository;

    private TransactionSerializer transactionSerializer;

    private RemotingCommandSerializer remotingCommandSerializer;

    private RegistryService registryService;

    private LoadBalanceServcie loadBalanceServcie;

    private volatile boolean isShutdown = false;

    public TccClient(ClientConfig clientConfig) {
        if (clientConfig != null) {
            this.clientConfig = clientConfig;
        }

        if (this.clientConfig.getKryoPoolSize() > 0) {
            this.transactionSerializer = new RegisterableKryoTransactionSerializer(this.clientConfig.getKryoPoolSize());
            this.transactionStoreSerializer = new RegisterableKryoTransactionStoreSerializer(this.clientConfig.getKryoPoolSize());
            this.remotingCommandSerializer = new RegisterableKryoRemotingCommandSerializer(this.clientConfig.getKryoPoolSize());

        } else {
            this.transactionSerializer = new RegisterableKryoTransactionSerializer();
            this.transactionStoreSerializer = new RegisterableKryoTransactionStoreSerializer();
            this.remotingCommandSerializer = new RegisterableKryoRemotingCommandSerializer();
        }

        if (this.clientConfig.getStorageType() == StorageType.REMOTING) {
            this.registryService = RegistryFactory.getInstance(this.clientConfig);
            this.loadBalanceServcie = LoadBalanceFactory.getInstance(this.clientConfig);
            remotingClient = new NettyRemotingClient(this.remotingCommandSerializer, this.clientConfig,
                    new ServerAddressLoader() {
                        @Override
                        public InetSocketAddress selectOne(String key) {
                            return loadBalanceServcie.select(registryService.lookup());
                        }

                        @Override
                        public List<InetSocketAddress> getAll(String key) {
                            return registryService.lookup();
                        }

                        @Override
                        public boolean isAvailableAddress(InetSocketAddress remoteAddress) {
                            List<InetSocketAddress> inetSocketAddresses = registryService.lookup();

                            if (CollectionUtils.isEmpty(inetSocketAddresses)) {
                                return false;
                            }

                            for (InetSocketAddress inetSocketAddress : inetSocketAddresses) {
                                if (inetSocketAddress.equals(remoteAddress)) {
                                    return true;
                                }
                            }
                            return false;
                        }
                    });
        }

        this.transactionStorage = TransactionStorageFactory.create(transactionStoreSerializer, this.clientConfig);

        this.transactionRepository = new DefaultTransactionRepository(this.clientConfig.getDomain(), transactionSerializer, this.transactionStorage);

        this.recoveryExecutor = new ClientRecoveryExecutor(transactionSerializer, this.transactionRepository);

        this.requestProcessor = new ClientRecoveryProcessor(this.recoveryExecutor);

        if (transactionRepository.supportRecovery() && this.clientConfig.isRecoveryEnabled()) {
            this.scheduler = new RecoveryScheduler(this.clientConfig);

            transactionStoreRecovery = new TransactionStoreRecovery(this.transactionStorage, this.recoveryExecutor, this.clientConfig);
            transactionStoreRecovery.setStoreMode(StoreMode.DISTRIBUTED);
        }

        this.transactionManager = new TransactionManager(this.transactionRepository);
    }

    @Override
    @PostConstruct
    public void start() throws Exception {
        this.isShutdown = false;
        if (this.clientConfig.getStorageType() == StorageType.REMOTING) {
            this.registryService.start();
            this.registryService.subscribe();
            initializeRemotingClient();
        }

        if (transactionRepository.supportRecovery() && this.clientConfig.isRecoveryEnabled()) {
            scheduler.start();
            scheduler.scheduleJob(this.clientConfig.getDomain());
        }
    }

    @Override
    @PreDestroy
    public void shutdown() throws Exception {

        this.isShutdown = true;

        if (this.clientConfig.getStorageType() == StorageType.REMOTING) {
            this.remotingClient.shutdown();
            this.remotingClient = null;

            if (this.registryService != null) {
                this.registryService.close();
                this.registryService = null;
            }
            this.loadBalanceServcie = null;
        }

        if (scheduler != null) {
            scheduler.shutdown(true);
        }

        if (this.requestProcessExecutor != null) {
            this.requestProcessExecutor.shutdown();
        }

        if (this.transactionStoreRecovery != null) {
            this.transactionStoreRecovery.close();
        }

        if (this.transactionRepository != null) {
            this.transactionRepository.close();
        }

        this.transactionSerializer = null;
        this.transactionStoreSerializer = null;
        this.remotingCommandSerializer = null;

        this.transactionManager = null;
        this.recoveryExecutor = null;
        this.requestProcessor = null;
        this.transactionStorage = null;
    }

    @Override
    public TransactionStoreRecovery getTransactionStoreRecovery() {
        return transactionStoreRecovery;
    }

    public TransactionManager getTransactionManager() {
        return transactionManager;
    }

    public ClientConfig getClientConfig() {
        return clientConfig;
    }

    private void initializeRemotingClient() {
        ((RemotingTransactionStorage) this.transactionStorage).setRemotingClient(this.remotingClient);

        this.requestProcessExecutor = new ThreadPoolExecutor(this.clientConfig.getRequestProcessThreadSize(),
                clientConfig.getRequestProcessThreadSize(),
                1000 * 60, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(this.clientConfig.getRequestProcessThreadQueueCapacity()),
                new ThreadFactory() {
                    private final AtomicInteger threadIndex = new AtomicInteger(0);

                    @Override
                    public Thread newThread(Runnable r) {
                        return new Thread(r, String.format("StoreTransactionThread_%d", threadIndex.getAndIncrement()));
                    }
                });

        this.remotingClient.registerDefaultProcessor(requestProcessor, this.requestProcessExecutor);

        this.remotingClient.registerChannelHandlers(new ReConnectToServerHandler());

        this.remotingClient.start();

        registerToServer();
    }

    private void registerToServer() {
        RemotingCommand registerCommand = RemotingCommand.createCommand(RemotingCommandCode.SERVICE_REQ, null);
        registerCommand.setServiceCode(RemotingServiceCode.REGISTER);
        registerCommand.setBody(clientConfig.getDomain().getBytes());
        try {
            remotingClient.invokeSync(clientConfig.getClusterName(), registerCommand, clientConfig.getRequestTimeoutMillis());
        } catch (Exception e) {
            logger.error("failled to register to server", e);
        }
    }

    @ChannelHandler.Sharable
    class ReConnectToServerHandler extends ChannelInboundHandlerAdapter {
        @Override
        public void channelUnregistered(ChannelHandlerContext ctx) {

            if (!isShutdown) {
                //try reconnect to server
                ctx.channel().eventLoop().schedule(new Runnable() {
                    @Override
                    public void run() {
                        registerToServer();
                    }
                }, clientConfig.getReconnectIntervalSeconds(), TimeUnit.SECONDS);
            }
        }
    }

    public TransactionStorage getTransactionStorage() {
        return transactionStorage;
    }
}
