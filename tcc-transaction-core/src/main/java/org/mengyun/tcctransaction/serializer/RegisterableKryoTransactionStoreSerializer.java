package org.mengyun.tcctransaction.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.google.common.collect.Lists;
import org.mengyun.tcctransaction.api.TransactionXid;
import org.mengyun.tcctransaction.storage.TransactionStore;
import org.mengyun.tcctransaction.utils.CollectionUtils;

import javax.transaction.xa.Xid;
import java.util.List;

public class RegisterableKryoTransactionStoreSerializer extends RegisterableKryoSerializer<TransactionStore> implements TransactionStoreSerializer {


    static List<Class> registeredClasses = Lists.newArrayList(
            Xid.class,
            TransactionXid.class,
            TransactionStore.class);

    public RegisterableKryoTransactionStoreSerializer() {
        this(registeredClasses);
    }

    public RegisterableKryoTransactionStoreSerializer(int initPoolSize) {
        this(initPoolSize, registeredClasses);
    }

    public RegisterableKryoTransactionStoreSerializer(List<Class> registerClasses) {
        super(CollectionUtils.merge(registeredClasses, registerClasses));
    }

    public RegisterableKryoTransactionStoreSerializer(int initPoolSize, List<Class> registerClasses) {
        super(initPoolSize, CollectionUtils.merge(registeredClasses, registerClasses));
    }

    public RegisterableKryoTransactionStoreSerializer(int initPoolSize, List<Class> registerClasses, boolean warnUnregisteredClasses) {
        super(initPoolSize, CollectionUtils.merge(registeredClasses, registerClasses), warnUnregisteredClasses);
    }

    protected void initHook(Kryo kryo) {
        super.initHook(kryo);
    }
}
