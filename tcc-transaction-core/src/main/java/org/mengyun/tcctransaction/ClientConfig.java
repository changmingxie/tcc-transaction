package org.mengyun.tcctransaction;

import org.mengyun.tcctransaction.discovery.registry.RegistryConfig;
import org.mengyun.tcctransaction.properties.CommonProperties;
import org.mengyun.tcctransaction.properties.remoting.NettyClientProperties;
import org.mengyun.tcctransaction.recovery.RecoveryConfig;
import org.mengyun.tcctransaction.remoting.netty.NettyClientConfig;
import org.mengyun.tcctransaction.storage.StoreConfig;

public class ClientConfig extends AbstractConfig implements RecoveryConfig, NettyClientConfig, StoreConfig, RegistryConfig {

    public final static ClientConfig DEFAULT = new ClientConfig();

    private NettyClientConfig nettyClientConfig = new NettyClientProperties();

    public ClientConfig() {
    }

    public ClientConfig(CommonProperties commonProperties, StoreConfig storeConfig, RecoveryConfig recoveryConfig, NettyClientConfig nettyClientConfig, RegistryConfig registryConfig) {
        super(commonProperties, storeConfig, recoveryConfig, nettyClientConfig, registryConfig);
        if (nettyClientConfig != null) {
            this.nettyClientConfig = nettyClientConfig;
        }
    }

    @Override
    public long getConnectTimeoutMillis() {
        return nettyClientConfig.getConnectTimeoutMillis();
    }

    @Override
    public int getChannelPoolMaxTotal() {
        return nettyClientConfig.getChannelPoolMaxTotal();
    }

    @Override
    public int getChannelPoolMaxIdlePerKey() {
        return nettyClientConfig.getChannelPoolMaxIdlePerKey();
    }

    @Override
    public int getChannelPoolMaxTotalPerKey() {
        return nettyClientConfig.getChannelPoolMaxTotalPerKey();
    }

    @Override
    public int getChannelPoolMinIdlePerKey() {
        return nettyClientConfig.getChannelPoolMinIdlePerKey();
    }

    @Override
    public long getChannelPoolMaxWaitMillis() {
        return nettyClientConfig.getChannelPoolMaxWaitMillis();
    }

    @Override
    public long getChannelPoolTimeBetweenEvictionRunsMillis() {
        return nettyClientConfig.getChannelPoolTimeBetweenEvictionRunsMillis();
    }

    @Override
    public int getNumTestsPerEvictionRun() {
        return nettyClientConfig.getNumTestsPerEvictionRun();
    }


    @Override
    public int getChannelMaxIdleTimeSeconds() {
        return nettyClientConfig.getChannelMaxIdleTimeSeconds();
    }

    @Override
    public int getReconnectIntervalSeconds() {
        return nettyClientConfig.getReconnectIntervalSeconds();
    }

    public void setNettyClientConfig(NettyClientConfig nettyClientConfig) {
        this.nettyClientConfig = nettyClientConfig;
        setNettyConfig(nettyClientConfig);
    }
}
