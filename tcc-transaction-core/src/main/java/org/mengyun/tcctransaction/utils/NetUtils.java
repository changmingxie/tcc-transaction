package org.mengyun.tcctransaction.utils;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

public class NetUtils {

    public static InetSocketAddress toInetSocketAddress(String address) {
        int i = address.indexOf(':');
        String host;
        int port;
        if (i > -1) {
            host = address.substring(0, i);
            port = Integer.parseInt(address.substring(i + 1));
        } else {
            host = address;
            port = 0;
        }
        return new InetSocketAddress(host, port);
    }

    public static String parseSocketAddress(SocketAddress socketAddress) {
        if (socketAddress != null) {
            String addr = socketAddress.toString();
            int index = addr.lastIndexOf("/");
            return (index != -1) ? addr.substring(index + 1) : addr;
        }
        return "";
    }
}
