package org.mengyun.tcctransaction;

import org.mengyun.tcctransaction.discovery.registry.RegistryFactory;
import org.mengyun.tcctransaction.discovery.registry.RegistryService;
import org.mengyun.tcctransaction.exception.SystemException;
import org.mengyun.tcctransaction.processor.ServerRecoveryExecutor;
import org.mengyun.tcctransaction.processor.ServerRequestProcessor;
import org.mengyun.tcctransaction.recovery.RecoveryExecutor;
import org.mengyun.tcctransaction.recovery.RecoveryScheduler;
import org.mengyun.tcctransaction.recovery.TransactionStoreRecovery;
import org.mengyun.tcctransaction.remoting.RemotingServer;
import org.mengyun.tcctransaction.remoting.RequestProcessor;
import org.mengyun.tcctransaction.remoting.netty.NettyRemotingServer;
import org.mengyun.tcctransaction.repository.StoreMode;
import org.mengyun.tcctransaction.serializer.RegisterableKryoTransactionStoreSerializer;
import org.mengyun.tcctransaction.serializer.TransactionStoreSerializer;
import org.mengyun.tcctransaction.storage.StorageType;
import org.mengyun.tcctransaction.storage.TransactionStorage;
import org.mengyun.tcctransaction.storage.TransactionStorageFactory;
import org.mengyun.tcctransaction.utils.NetUtils;
import org.mengyun.tcctransaction.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TccServer implements TccService {

    static final Logger logger = LoggerFactory.getLogger(TccServer.class.getSimpleName());

    private ServerConfig serverConfig = ServerConfig.DEFAULT;

    private TransactionStorage transactionStorage;

    private RecoveryExecutor recoveryExecutor;

    private RequestProcessor requestProcessor;

    private TransactionStoreRecovery transactionStoreRecovery;

    private ExecutorService requestProcessExecutor;

    private TransactionStoreSerializer transactionStoreSerializer;

    private RecoveryScheduler scheduler;

    private RemotingServer remotingServer;

    private RegistryService registryService;

    public TccServer(ServerConfig serverConfig) {
        if (serverConfig != null) {
            this.serverConfig = serverConfig;
        }

        if (this.serverConfig.getKryoPoolSize() > 0) {
            this.transactionStoreSerializer = new RegisterableKryoTransactionStoreSerializer(this.serverConfig.getKryoPoolSize());
        } else {
            this.transactionStoreSerializer = new RegisterableKryoTransactionStoreSerializer();
        }

        if (this.serverConfig.getStorageType() == StorageType.REMOTING) {
            throw new SystemException(String.format("unsupported StorageType<%s> in server side.", this.serverConfig.getStorageType().value()));
        }

        this.remotingServer = new NettyRemotingServer(this.serverConfig);

        this.registryService = RegistryFactory.getInstance(this.serverConfig);

        this.transactionStorage = TransactionStorageFactory.create(transactionStoreSerializer, this.serverConfig);

        this.scheduler = new RecoveryScheduler(this.serverConfig);

        this.recoveryExecutor = new ServerRecoveryExecutor(this.scheduler, this.transactionStoreSerializer, this.transactionStorage);

        this.requestProcessor = new ServerRequestProcessor(this.transactionStoreSerializer, this.recoveryExecutor, this.transactionStorage);

        this.transactionStoreRecovery = new TransactionStoreRecovery(this.transactionStorage, this.recoveryExecutor, this.serverConfig);
        this.transactionStoreRecovery.setStoreMode(StoreMode.CENTRAL);
    }

    @Override
    @PostConstruct
    public void start() throws Exception {
        initializeRemotingServer();

        initializeRegistry();

        initializeRecoverySchedule();
    }

    @Override
    @PreDestroy
    public void shutdown() throws Exception {

        if (this.registryService != null) {
            this.registryService.close();
        }

        if (this.remotingServer != null) {
            this.remotingServer.shutdown();
        }

        if (this.scheduler != null) {
            this.scheduler.shutdown(true);
        }

        if (this.requestProcessExecutor != null) {
            this.requestProcessExecutor.shutdown();
        }

        if (this.transactionStoreRecovery != null) {
            this.transactionStoreRecovery.close();
        }

        this.transactionStoreSerializer = null;

        this.recoveryExecutor = null;
        this.requestProcessor = null;
        this.transactionStorage = null;
    }

    @Override
    public TransactionStoreRecovery getTransactionStoreRecovery() {
        return transactionStoreRecovery;
    }

    public ServerConfig getServerConfig() {
        return serverConfig;
    }

    private void initializeRemotingServer() {

        this.requestProcessExecutor = new ThreadPoolExecutor(serverConfig.getRequestProcessThreadSize(),
                serverConfig.getRequestProcessThreadSize(),
                1000 * 60, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(this.serverConfig.getRequestProcessThreadQueueCapacity()),
                new ThreadFactory() {
                    private AtomicInteger threadIndex = new AtomicInteger(0);

                    @Override
                    public Thread newThread(Runnable r) {
                        return new Thread(r, String.format("StoreTransactionThread_%d", threadIndex.getAndIncrement()));
                    }
                });

        remotingServer.registerDefaultProcessor(this.requestProcessor, this.requestProcessExecutor);

        remotingServer.start();
    }


    private void initializeRecoverySchedule() {
        if (this.serverConfig.isRecoveryEnabled()) {
            scheduler.start();
        }
    }

    private void initializeRegistry() throws UnknownHostException {
        InetSocketAddress inetSocketAddress;
        if (StringUtils.isNotEmpty(this.serverConfig.getServerAddress())) {
            inetSocketAddress = NetUtils.toInetSocketAddress(this.serverConfig.getServerAddress());
        } else {
            inetSocketAddress = new InetSocketAddress(InetAddress.getLocalHost(), serverConfig.getListenPort());
        }
        this.registryService.start();
        this.registryService.register(inetSocketAddress);
    }

    public RecoveryScheduler getScheduler() {
        return scheduler;
    }

    public TransactionStorage getTransactionStorage() {
        return transactionStorage;
    }
}
