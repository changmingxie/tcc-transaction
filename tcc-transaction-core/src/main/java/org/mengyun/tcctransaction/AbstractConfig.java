package org.mengyun.tcctransaction;

import org.mengyun.tcctransaction.discovery.registry.RegistryConfig;
import org.mengyun.tcctransaction.discovery.registry.RegistryType;
import org.mengyun.tcctransaction.discovery.registry.direct.DirectRegistryProperties;
import org.mengyun.tcctransaction.discovery.registry.nacos.NacosRegistryProperties;
import org.mengyun.tcctransaction.discovery.registry.zookeeper.ZookeeperRegistryProperties;
import org.mengyun.tcctransaction.properties.CommonProperties;
import org.mengyun.tcctransaction.properties.RecoveryProperties;
import org.mengyun.tcctransaction.properties.RegistryProperties;
import org.mengyun.tcctransaction.properties.remoting.NettyProperties;
import org.mengyun.tcctransaction.properties.store.StoreProperties;
import org.mengyun.tcctransaction.recovery.RecoveryConfig;
import org.mengyun.tcctransaction.remoting.netty.NettyConfig;
import org.mengyun.tcctransaction.storage.StorageType;
import org.mengyun.tcctransaction.storage.StoreConfig;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ShardedJedisPool;

import javax.sql.DataSource;

public class AbstractConfig implements StoreConfig, RecoveryConfig, NettyConfig, RegistryConfig {

    //AppCommonConfig
    private CommonProperties commonProperties = new CommonProperties();
    //StoreConfig
    private StoreConfig storeConfig = new StoreProperties();
    //RecoveryConfig
    private RecoveryConfig recoveryConfig = new RecoveryProperties();
    //NettyConfig
    private NettyConfig nettyConfig = new NettyProperties();
    //RegistyConfig
    private RegistryConfig registryConfig = new RegistryProperties();

    public AbstractConfig() {
    }

    public AbstractConfig(CommonProperties commonProperties, StoreConfig storeConfig, RecoveryConfig recoveryConfig, NettyConfig nettyConfig, RegistryConfig registryConfig) {
        if (commonProperties != null) {
            this.commonProperties = commonProperties;
        }
        if (storeConfig != null) {
            this.storeConfig = storeConfig;
        }
        if (recoveryConfig != null) {
            this.recoveryConfig = recoveryConfig;
        }
        if (nettyConfig != null) {
            this.nettyConfig = nettyConfig;
        }
        if (recoveryConfig != null) {
            this.registryConfig = registryConfig;
        }
    }

    @Override
    public String getTbSuffix() {
        return storeConfig.getTbSuffix();
    }

    @Override
    public DataSource getDataSource() {
        return storeConfig.getDataSource();
    }

    @Override
    public String getDomain() {
        return storeConfig.getDomain();
    }

    @Override
    public StorageType getStorageType() {
        return storeConfig.getStorageType();
    }

    @Override
    public int getKryoPoolSize() {
        return storeConfig.getKryoPoolSize();
    }

    @Override
    public String getTransactionStorageClass() {
        return storeConfig.getTransactionStorageClass();
    }

    @Override
    public long getRequestTimeoutMillis() {
        return storeConfig.getRequestTimeoutMillis();
    }

    @Override
    public String getLocation() {
        return storeConfig.getLocation();
    }

    @Override
    public JedisPool getJedisPool() {
        return storeConfig.getJedisPool();
    }

    @Override
    public JedisCluster getJedisCluster() {
        return storeConfig.getJedisCluster();
    }

    @Override
    public ShardedJedisPool getShardedJedisPool() {
        return storeConfig.getShardedJedisPool();
    }

    @Override
    public String getServerAddress() {
        return storeConfig.getServerAddress();
    }

    public int getRequestProcessThreadSize() {
        return commonProperties.getRequestProcessThreadSize();
    }

    public int getRequestProcessThreadQueueCapacity() {
        return commonProperties.getRequestProcessThreadQueueCapacity();
    }

    @Override
    public int getMaxRetryCount() {
        return recoveryConfig.getMaxRetryCount();
    }

    @Override
    public int getRecoverDuration() {
        return recoveryConfig.getRecoverDuration();
    }

    @Override
    public String getCronExpression() {
        return recoveryConfig.getCronExpression();
    }

    @Override
    public int getFetchPageSize() {
        return recoveryConfig.getFetchPageSize();
    }

    @Override
    public int getConcurrentRecoveryThreadCount() {
        return recoveryConfig.getConcurrentRecoveryThreadCount();
    }

    @Override
    public boolean isRecoveryEnabled() {
        return recoveryConfig.isRecoveryEnabled();
    }

    @Override
    public String getQuartzDataSourceDriver() {
        return recoveryConfig.getQuartzDataSourceDriver();
    }

    @Override
    public String getQuartzDataSourceUrl() {
        return recoveryConfig.getQuartzDataSourceUrl();
    }

    @Override
    public String getQuartzDataSourceUser() {
        return recoveryConfig.getQuartzDataSourceUser();
    }

    @Override
    public String getQuartzDataSourcePassword() {
        return recoveryConfig.getQuartzDataSourcePassword();
    }

    @Override
    public String getQuartzDataSourceValidationQuery() {
        return recoveryConfig.getQuartzDataSourceValidationQuery();
    }

    @Override
    public boolean isQuartzClustered() {
        return recoveryConfig.isQuartzClustered();
    }

    @Override
    public boolean isUpdateJobForcibly() {
        return recoveryConfig.isUpdateJobForcibly();
    }

    @Override
    public int getWorkerThreadSize() {
        return nettyConfig.getWorkerThreadSize();
    }

    @Override
    public int getSocketBacklog() {
        return nettyConfig.getSocketBacklog();
    }

    @Override
    public int getSocketRcvBufSize() {
        return nettyConfig.getSocketRcvBufSize();
    }

    @Override
    public int getSocketSndBufSize() {
        return nettyConfig.getSocketSndBufSize();
    }

    @Override
    public int getFrameMaxLength() {
        return nettyConfig.getFrameMaxLength();
    }

    @Override
    public int getWorkSelectorThreadSize() {
        return nettyConfig.getWorkSelectorThreadSize();
    }

    @Override
    public String getClusterName() {
        return registryConfig.getClusterName();
    }

    @Override
    public ZookeeperRegistryProperties getZookeeperRegistryProperties() {
        return registryConfig.getZookeeperRegistryProperties();
    }

    @Override
    public NacosRegistryProperties getNacosRegistryProperties() {
        return registryConfig.getNacosRegistryProperties();
    }

    @Override
    public DirectRegistryProperties getDirectRegistryProperties() {
        return registryConfig.getDirectRegistryProperties();
    }

    @Override
    public RegistryType getRegistryType() {
        return registryConfig.getRegistryType();
    }

    @Override
    public String getCustomRegistryName() {
        return registryConfig.getCustomRegistryName();
    }

    @Override
    public String getLoadBalanceType() {
        return registryConfig.getLoadBalanceType();
    }

    public void setCommonProperties(CommonProperties commonProperties) {
        this.commonProperties = commonProperties;
    }

    public void setStoreConfig(StoreConfig storeConfig) {
        this.storeConfig = storeConfig;
    }

    public void setRecoveryConfig(RecoveryConfig recoveryConfig) {
        this.recoveryConfig = recoveryConfig;
    }

    protected void setNettyConfig(NettyConfig nettyConfig) {
        this.nettyConfig = nettyConfig;
    }

    public void setRegistryConfig(RegistryConfig registryConfig) {
        this.registryConfig = registryConfig;
    }
}
