package org.mengyun.tcctransaction.properties.store;

import org.mengyun.tcctransaction.storage.StorageType;
import org.mengyun.tcctransaction.storage.StoreConfig;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ShardedJedisPool;

import javax.sql.DataSource;

/**
 * @author Nervose.Wu
 * @date 2022/5/24 09:45
 */
public class StoreProperties implements StoreConfig {

    private StorageType storageType = StorageType.MEMORY;
    private int kryoPoolSize = 512;
    private String domain = "TCC";
    private String transactionStorageClass;
    //the timout of remoting storage request
    private long requestTimeoutMillis = 2000L;
    private String location = "/tmp";
    private String tbSuffix;
    private String serverAddress = "127.0.0.1:2332";
    private JdbcStoreProperties jdbc = new JdbcStoreProperties();
    private RedisStoreProperties redis = new RedisStoreProperties();
    private ShardRedisStoreProperties shardRedis = new ShardRedisStoreProperties();
    private RedisClusterStoreProperties redisCluster = new RedisClusterStoreProperties();


    @Override
    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    @Override
    public int getKryoPoolSize() {
        return kryoPoolSize;
    }

    public void setKryoPoolSize(int kryoPoolSize) {
        this.kryoPoolSize = kryoPoolSize;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String getTransactionStorageClass() {
        return transactionStorageClass;
    }

    public void setTransactionStorageClass(String transactionStorageClass) {
        this.transactionStorageClass = transactionStorageClass;
    }

    @Override
    public long getRequestTimeoutMillis() {
        return requestTimeoutMillis;
    }

    public void setRequestTimeoutMillis(long requestTimeoutMillis) {
        this.requestTimeoutMillis = requestTimeoutMillis;
    }

    @Override
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String getTbSuffix() {
        return tbSuffix;
    }

    public void setTbSuffix(String tbSuffix) {
        this.tbSuffix = tbSuffix;
    }

    @Override
    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public JdbcStoreProperties getJdbc() {
        return jdbc;
    }

    public void setJdbc(JdbcStoreProperties jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public JedisPool getJedisPool() {
        return redis.getJedisPool();
    }


    @Override
    public ShardedJedisPool getShardedJedisPool() {
        return shardRedis.getShardedJedisPool();
    }


    @Override
    public JedisCluster getJedisCluster() {
        return redisCluster.getCluster();
    }

    @Override
    public DataSource getDataSource() {
        return jdbc.getDataSource();
    }

    public RedisStoreProperties getRedis() {
        return redis;
    }

    public void setRedis(RedisStoreProperties redis) {
        this.redis = redis;
    }

    public ShardRedisStoreProperties getShardRedis() {
        return shardRedis;
    }

    public void setShardRedis(ShardRedisStoreProperties shardRedis) {
        this.shardRedis = shardRedis;
    }

    public RedisClusterStoreProperties getRedisCluster() {
        return redisCluster;
    }

    public void setRedisCluster(RedisClusterStoreProperties redisCluster) {
        this.redisCluster = redisCluster;
    }
}
