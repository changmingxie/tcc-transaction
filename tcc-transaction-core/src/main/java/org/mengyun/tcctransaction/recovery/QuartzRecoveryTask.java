package org.mengyun.tcctransaction.recovery;

import org.mengyun.tcctransaction.TccService;
import org.mengyun.tcctransaction.support.FactoryBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class QuartzRecoveryTask implements Job {
    public final static String DOMAIN = "DOMAIN";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String domain = context.getJobDetail().getJobDataMap().getString(DOMAIN);
        FactoryBuilder.factoryOf(TccService.class).getInstance().getTransactionStoreRecovery().startRecover(domain);
    }
}
