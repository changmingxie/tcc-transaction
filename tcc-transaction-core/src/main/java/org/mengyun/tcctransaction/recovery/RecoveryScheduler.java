package org.mengyun.tcctransaction.recovery;

import org.mengyun.tcctransaction.exception.SystemException;
import org.quartz.*;

import java.util.Properties;

/**
 * Created by changming.xie on 6/2/16.
 */
public class RecoveryScheduler {

    public final static String JOB_NAME = "TccServerRecoverJob_%s";

    public final static String TRIGGER_NAME = "TccServerRecoveryTrigger_%s";

    private Scheduler scheduler;

    private RecoveryConfig recoveryConfig;

    public RecoveryScheduler(RecoveryConfig recoveryConfig) {
        this.scheduler = getScheduler(recoveryConfig);
        this.recoveryConfig = recoveryConfig;
    }

    public void scheduleJob(String domain) {

        String jobName = String.format(JOB_NAME, domain);
        String triggerName = String.format(TRIGGER_NAME, domain);

        JobDetail jobDetail = JobBuilder.newJob(QuartzRecoveryTask.class).withIdentity(jobName).build();
        jobDetail.getJobDataMap().put(QuartzRecoveryTask.DOMAIN, domain);
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(triggerName)
                .withSchedule(CronScheduleBuilder.cronSchedule(recoveryConfig.getCronExpression())
                        .withMisfireHandlingInstructionDoNothing()).build();

        try {
            if (!scheduler.checkExists(JobKey.jobKey(jobName))) {
                scheduler.scheduleJob(jobDetail, cronTrigger);
            } else {
                if (recoveryConfig.isUpdateJobForcibly()) {
                    scheduler.deleteJob(JobKey.jobKey(jobName));
                    scheduler.scheduleJob(jobDetail, cronTrigger);
                }
            }
        } catch (SchedulerException e) {
            throw new SystemException(String.format("register recovery task for domain<%s> failed", domain), e);
        }
    }

    public void start() {

        try {
            scheduler.startDelayed(recoveryConfig.getRecoverDuration());
        } catch (SchedulerException e) {
            throw new SystemException("quartz schedule job start failed", e);
        }
    }

    private Scheduler getScheduler(RecoveryConfig recoveryConfig) {
        Properties conf = new Properties();
        conf.put("org.quartz.scheduler.instanceName", "recovery-quartz-scheduler");
        conf.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        conf.put("org.quartz.threadPool.threadCount", String.valueOf(Runtime.getRuntime().availableProcessors()));
        conf.put("org.quartz.scheduler.skipUpdateCheck", "false");

        if (recoveryConfig.isQuartzClustered()) {
            conf.put("org.quartz.jobStore.isClustered", String.valueOf(recoveryConfig.isQuartzClustered()));
            conf.put("org.quartz.scheduler.instanceId", "AUTO");
            conf.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
            conf.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
            conf.put("org.quartz.jobStore.useProperties", "false");
            conf.put("org.quartz.jobStore.dataSource", "myDS");
            conf.put("org.quartz.jobStore.tablePrefix", "QRTZ_");
            conf.put("org.quartz.dataSource.myDS.driver", recoveryConfig.getQuartzDataSourceDriver());
            conf.put("org.quartz.dataSource.myDS.URL", recoveryConfig.getQuartzDataSourceUrl());
            conf.put("org.quartz.dataSource.myDS.user", recoveryConfig.getQuartzDataSourceUser());
            conf.put("org.quartz.dataSource.myDS.password", recoveryConfig.getQuartzDataSourcePassword());
//            conf.put("org.quartz.dataSource.myDS.maxConnections", "10");
            conf.put("org.quartz.dataSource.myDS.validationQuery", recoveryConfig.getQuartzDataSourceValidationQuery());
            conf.put("org.quartz.jobStore.misfireThreshold", "1000");
        }

        try {
            SchedulerFactory factory = new org.quartz.impl.StdSchedulerFactory(conf);
            return factory.getScheduler();
        } catch (SchedulerException e) {
            throw new SystemException("initialize recovery scheduler failed", e);
        }
    }

    public void shutdown(boolean waitForComplete) throws SchedulerException {
        this.scheduler.shutdown(waitForComplete);
    }

    public Scheduler getScheduler() {
        return scheduler;
    }
}
